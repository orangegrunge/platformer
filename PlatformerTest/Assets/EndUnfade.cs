using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndUnfade : MonoBehaviour
{
    private SessionManager sessionManager;

    private void Start()
    {
        sessionManager = FindObjectOfType<SessionManager>();
        sessionManager.FadeOut();
    }
}
