using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    public float duration;
    public float power;
    public float shakeFadeTime;
    float shakeRotation;
    public float rotationMultiplier;
    public float dumpingRate;


    [Header("Debug")]
    public float timeLeft;
    float shakePower;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }


    private void Update()
    {

        transform.position = Vector3.Lerp(transform.position, new Vector3(-0.5f, -0.5f, -10), dumpingRate * Time.deltaTime / 0.003f);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, dumpingRate * Time.deltaTime / 0.003f);
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.H)) StartShake();

        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;

            float xAmount = Random.Range(-1f, 1f) * shakePower;
            float yAmount = Random.Range(-1f, 1f) * shakePower;

            transform.position += new Vector3(xAmount, yAmount, 0f);
            transform.rotation = Quaternion.Euler(0f, 0f, shakeRotation * Random.Range(-1f, 1f));

            shakePower = Mathf.MoveTowards(shakePower, 0f, shakeFadeTime * Time.deltaTime);
            shakeRotation = Mathf.MoveTowards(shakeRotation, 0f, shakeFadeTime * Time.deltaTime * rotationMultiplier);
        }
        
    }

    public void StartShake()
    {
        timeLeft = duration;
        shakePower = power;
        shakeRotation = power;
    }

    
}
