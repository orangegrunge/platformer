using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[ExecuteInEditMode]
public class Hazard : MonoBehaviour
{

    public enum Tag
    {
        SPIKE,
        FIRE
    }

    public enum Side
    {
        TOP,
        SIDE,
        BOTTOM,
        ALL
    }

    public static List<string> GetTags()
    {
        return Enum.GetNames(typeof(Tag)).ToList<String>();
    }
    
    public static bool IsHazard(string tag)
    {
        return GetTags().Contains(tag);
    }

    public Tag hazardTag;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (!Application.isPlaying)
        {
            
        }
        //Debug.Log(transform.up);
    }


}
