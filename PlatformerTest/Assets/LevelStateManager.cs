using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelStateManager : MonoBehaviour
{
    public int initialLives;
    public float levelDuration;
    int currentLives;
    float timeLeft;

    //[HideInInspector]
    public bool pause;
    DebugUIController debugUI;
    BellManager bellManager;
    ScreenShake screenShake;
    SessionManager sessionManager;
    DialogController dialogController;

    public bool win;
    public bool loose;

    private void Start()
    {
        sessionManager = FindObjectOfType<SessionManager>();
        sessionManager.FadeOut();

        dialogController = FindObjectOfType<DialogController>();

        if (sessionManager.newScene) pause = true;
        currentLives = initialLives;
        timeLeft = levelDuration;
        debugUI = FindObjectOfType<DebugUIController>();
        debugUI.InitLives(currentLives);
        debugUI.InitTimer(timeLeft);

        


        bellManager = FindObjectOfType<BellManager>();
        screenShake = FindObjectOfType<ScreenShake>();

    }

    public void Update()
    {
        if (!pause)
        {
            timeLeft -= Time.deltaTime;
            debugUI.UpdateTimer(timeLeft);
            if (timeLeft <= 0 && !win) Win();
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.P)) timeLeft = 2;
    }

    private void Win()
    {
        win = true;
        bellManager.Stop();
    }

    public void Miss()
    {
        //looseLife();
    }

    public void GetHit(bool oneHit)
    {
        looseLife(oneHit);
    }

    public void looseLife(bool oneHit)
    {
        screenShake.StartShake();
        if (!oneHit)
            currentLives--;
        else currentLives = 0;
        debugUI.LoseLife();
        if (currentLives <= 0) Loose();

    }

    public void Loose()
    {
        loose = true;
        dialogController.ShowSpeech("restart_text");
    }

    public void SetPause(bool paused)
    {
        pause = paused;
    }

    internal float GetTime()
    {
        return levelDuration - timeLeft;
    }
}
