using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExplode : MonoBehaviour
{


    internal void Explode()
    {
        HazardController projectile = GetComponent<HazardController>();
        if (projectile) projectile.Explode();
    }
}
