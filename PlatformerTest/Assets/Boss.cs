using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

    public MobileSpawner spawner;
    public Animator animator;

    public void Spawn()
    {
        animator.SetTrigger("attack");
        spawner.Spawn();
    }
    
}
