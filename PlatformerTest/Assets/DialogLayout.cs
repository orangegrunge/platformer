using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DialogPadding
{
    public float left, right, top, bottom, between;
}


[ExecuteInEditMode]
public class DialogLayout : MonoBehaviour
{
    [Header("Refs")]
    public RectTransform rectTransform;
    public RectTransform face;
    public RectTransform text;

    [Header("Settings")]
    public DialogPadding padding;
    
    void Update()
    {
        face.anchoredPosition = new Vector2(padding.left, -padding.top);
        float faceHeight = rectTransform.sizeDelta.y - padding.top - padding.bottom;
        face.sizeDelta = Vector2.one * faceHeight;

        float textOffset = padding.left + padding.between + face.sizeDelta.x;
        text.anchoredPosition = new Vector2(textOffset, -padding.top);
        float textWidth = rectTransform.rect.width - textOffset - padding.right;
        text.sizeDelta = new Vector2(textWidth, faceHeight);
    }
}
