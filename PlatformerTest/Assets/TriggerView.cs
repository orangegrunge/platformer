using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(Trigger))]
[ExecuteInEditMode]
public class TriggerView : MonoBehaviour
{
    [HideInInspector]
    public bool isActivated;
    public Trigger trigger;
    public Animator animator;

    private void Start()
    {
        UpdateState();
    }

    private void Update()
    {
        if (isActivated != trigger.isActivated) {
            UpdateState();
        }

    }

    private void UpdateState()
    {
        animator.SetBool("isActivated", trigger.isActivated);
        isActivated = trigger.isActivated;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        string state = isActivated ? "ON" : "OFF";
        var labelPos = (Vector2)transform.position + Vector2.up * 1f + Vector2.right * 0.2f;
        var markerPos = labelPos + Vector2.left * 0.1f;

        GUIStyle st = new GUIStyle();
        st.normal.textColor = isActivated ? Color.green : Color.red;

        if (!trigger.settings.controlledBySwitch)
        {
            Handles.Label(labelPos, state, st);
            Gizmos.color = isActivated ? Color.green : Color.red;
            Gizmos.DrawSphere(markerPos, 0.1f);
        }
        else
        {
            Handles.Label(labelPos, "SWITCH");
        }
    }
#endif
}
