using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonTrigger : MonoBehaviour
{
    public float triggerTime;
    public bool triggerByPlayer;
    public bool moveUnderground;
    public float riseTime;
    public Collider2D riseTriggerCollider;

    bool hidden;
    bool rising;
    bool done;
    [HideInInspector] public bool playerIsNear;

    LevelStateManager stateManager;
    BellHorizontalController horizontalController;
    HazardController hazardController;
    Collider2D hazardCollider;
    public Animator animator;

    private void Start()
    {
        hidden = true;
        stateManager = FindObjectOfType<LevelStateManager>();
        horizontalController = GetComponent<BellHorizontalController>();
        hazardController = GetComponentInChildren<HazardController>();
        hazardCollider = hazardController.GetComponent<Collider2D>();
    }

    private void Update()
    {
        if (!stateManager.pause)
        {
            
            if (!done && !rising && triggerTime <= stateManager.GetTime() && (!triggerByPlayer || playerIsNear))
            {
                rising = true;
                hidden = false;
                animator.SetTrigger("rise");
                triggerTime = stateManager.GetTime();

            }
            if (rising && triggerTime + riseTime <= stateManager.GetTime())
            {
                rising = false;
                hidden = false;
                done = true;
            }

            HoldMovement();
            HoldHazard();
        }
        else
        {

        }
    }




    private void HoldMovement()
    {
        if (!horizontalController) return;

        if (rising || (hidden && !moveUnderground))
        {
            horizontalController.hold = true;

        }
        else
        {
            horizontalController.hold = false;
        }
    }

    private void HoldHazard()
    {
        if (!hazardCollider) return;

        if (!rising && !hidden)
        {
            hazardCollider.enabled = true;

        }
        else
        {
            hazardCollider.enabled = false;
        }
    }
}
