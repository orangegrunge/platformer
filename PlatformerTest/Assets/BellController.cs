using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class BellController : MonoBehaviour
{
    public int id;
    BellManager bellManager;
    bool active;
    float timer;
    ActionSpot actionSpot;
    AudioSource audioSource;

    public AudioClip hitSound;
    public AudioClip missSound;
    public MobileSpawner spawner;
    [Header("Debug")]
    public SpriteRenderer spriteRenderer;
    public Color idleColor;
    public Color activeColor;
    public Color dangerColor;
    public Color missColor;
    public float missTime;
    public float ringTime;
    bool missed;
    public Animator animator;

    public ParticleSystem particleSystem1;
    public ParticleSystem particleSystem2;

    private void Start()
    {
        actionSpot = GetComponent<ActionSpot>();
        audioSource = GetComponent<AudioSource>();
        actionSpot.active = false;
        bellManager = FindObjectOfType<BellManager>();
        bellManager.RegisterBell(this);
        //spriteRenderer.color = idleColor;
        particleSystem1.startColor = idleColor;
        particleSystem2.startColor = idleColor;
    }

    public void Ring()
    {
        DoSound();
        animator.SetTrigger("ring");
        StartCoroutine(SkipRingTime());
        GoToNext();
        var boss = FindObjectOfType<Boss>();
        if (boss) boss.Spawn(); 
    }

    public void DoSound()
    {
        audioSource.PlayOneShot(hitSound);
    }

    private void GoToNext()
    {
        Deactivate();
        bellManager.Ring();
    }

    private void Miss()
    {
        spawner.Spawn();
        audioSource.PlayOneShot(missSound);
        GoToNext();
        missed = true;
        // spriteRenderer.color = missColor;
        particleSystem1.startColor = missColor;
        particleSystem2.startColor = missColor;
        StartCoroutine(SkipMissed());
    }

    IEnumerator SkipRingTime()
    {
        yield return new WaitForSeconds(ringTime);
        animator.SetTrigger("stop");
    }

    IEnumerator SkipMissed()
    {
        yield return new WaitForSeconds(missTime);
        missed = false;
        if (active)
        {
            //spriteRenderer.color = activeColor;

            particleSystem1.startColor = activeColor;
            particleSystem2.startColor = activeColor;
        }
        else
        {
            //spriteRenderer.color = idleColor;
            particleSystem1.startColor = idleColor;
            particleSystem2.startColor = idleColor;
        }
    }

    public void Activate()
    {
        if (!missed)
        {
            //spriteRenderer.color = activeColor;
            particleSystem1.Play();
            particleSystem2.Play();
            particleSystem1.startColor = activeColor;
            particleSystem2.startColor = activeColor;
        }
        timer = 0;
        active = true;
        actionSpot.active = true;
    }

    public void Deactivate()
    {
        timer = 0;
        active = false;
        actionSpot.active = false;
        particleSystem1.startColor = idleColor;
        particleSystem2.startColor = idleColor;

    }

    private void Update()
    {
        if (active)
        {
            OperateTimer();
        }
    }

    void OperateTimer()
    {
        if (bellManager.pause) return;

        timer += Time.deltaTime;
        if (timer > bellManager.tempo)
        {
            Miss();
            bellManager.Miss();
        }
        else
        {
            if (!missed) ChangeTimerView();
        }
    }

    void ChangeTimerView()
    {
        var t = Mathf.Clamp01(timer / bellManager.tempo);
        particleSystem1.startColor = Color.Lerp(activeColor, dangerColor, t);
        particleSystem2.startColor = Color.Lerp(activeColor, dangerColor, t);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        var c = Handles.color;
        GUIStyle red = new GUIStyle();
        red.normal.textColor = Color.red;
        red.fontSize = 20;
        Handles.Label(transform.position + new Vector3(0.2f, 1f, 0), id.ToString(), red);
    }
#endif
}
