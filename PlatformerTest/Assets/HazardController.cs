using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardController : MonoBehaviour
{
    public bool destroyOnPlayerHit;
    PlayerController playerController;
    public GameObject explosion;
    public float shrinkTime;
    bool shrink;
    public float shrinkRate;
    public Projectile projectile;
    public bool ignoreBlink;
    public bool oneHit;
    private bool needToDestroy;

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (playerController && (!playerController.blink || ignoreBlink))
            {
                playerController.GetHit(oneHit);
                if (destroyOnPlayerHit)
                {
                    if (!needToDestroy) Explode();
                }
            }
        }
    }

    private void Update()
    {
        if (shrink)
        {
            transform.localScale -= Vector3.one * shrinkRate * Time.deltaTime; 
        }
    }

    public void Explode()
    {
        needToDestroy = true;
        if (projectile) projectile.Stop();

        explosion.transform.parent = null;
        explosion.SetActive(true);

        Destroy(gameObject, shrinkTime);
    }
}
