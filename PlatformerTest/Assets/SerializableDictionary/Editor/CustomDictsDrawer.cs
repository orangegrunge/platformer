using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(HazardTagHazardSideEventDict))]
public class HazardTagHazardSideEventDrawer: SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(HazardSideEventDict))]
public class HazardSideEventDrawer : SerializableDictionaryPropertyDrawer { }