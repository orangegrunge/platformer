using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class HazardSideEventDict : SerializableDictionary<string, UnityEvent> { }

[System.Serializable]
public class HazardTagHazardSideEventDict : SerializableDictionary<Hazard.Tag, List<HazardReactionEvent>> {}