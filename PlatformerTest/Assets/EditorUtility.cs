using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;



public class EditorUtility  
{

    public static void DrawIcon(GameObject gameObject, int idx)
    {
#if UNITY_EDITOR
        var largeIcons = GetTextures("sv_icon_dot", "_sml", 0, 8);
        var icon = largeIcons[idx];
        var egu = typeof(EditorGUIUtility);
        var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
        var args = new object[] { gameObject, icon.image };
        var setIcon = egu.GetMethod("SetIconForObject", flags, null, new Type[] { typeof(UnityEngine.Object), typeof(Texture2D) }, null);
        setIcon.Invoke(null, args);
#endif
    }
    private static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
    {

        GUIContent[] array = new GUIContent[count];
#if UNITY_EDITOR
        for (int i = 0; i < count; i++)
        {
            array[i] = EditorGUIUtility.IconContent(baseName + (startIndex + i) + postFix);
        }
#endif
        return array;

    }
}
