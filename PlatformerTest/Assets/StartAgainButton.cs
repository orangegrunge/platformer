using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAgainButton : MonoBehaviour
{
    
    public void StartAgain()
    {
        FindObjectOfType<SessionManager>().StartAgain();
    }

}
