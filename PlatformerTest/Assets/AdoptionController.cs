using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AdoptionController : MonoBehaviour
{
    public HashSet<StepChild> stepChildren = new HashSet<StepChild>();
    public List<StepChild> children;

    internal void AddChild(StepChild stepChild)
    {
        if (!stepChildren.Contains(stepChild))
        {
            stepChildren.Add(stepChild);
            stepChild.offset = stepChild.transform.position - transform.position;
            //AddComponent<ShadowTile>(stepChild);
        }

        
    }

    internal void RemoveChild(StepChild stepChild)
    {
        if (stepChildren.Contains(stepChild))
        {
            stepChildren.Remove(stepChild);
            stepChild.offset = Vector2.zero;
            //RemoveComponent<ShadowTile>(stepChild);
        }
    }

    private void Update()
    {
        if (!Application.isPlaying)
        {
            stepChildren.Remove(null);
        }

        children = GetChildren();
        
        if (Application.isPlaying)
        {
            children.ForEach(x => x.transform.position = (Vector2)transform.position + x.offset);
        }


    }

    private List<StepChild> GetChildren()
    {
        return new List<StepChild>(stepChildren);
    }
    /*
    private void AddComponent<T>(StepChild stepChild) where T: Component
    {
        if (GetComponent<T>() && !stepChild.mainObject.GetComponent<T>()) stepChild.mainObject.AddComponent<T>();
    }

    private void RemoveComponent<T>(StepChild stepChild) where T : Component
    {
        if (GetComponent<T>() && stepChild.mainObject.GetComponent<T>()) DestroyImmediate(stepChild.mainObject.GetComponent<T>());
    }
    */
    public List<T> GetAdoptedComponents<T>() where T : Component
    {
        var children = GetChildren();
        var result = new List<T>();
        children.ForEach(x =>{
            if (x)
            {
                T c = x.mainObject.GetComponent<T>();
                if (c) result.Add(c);
            }
        });
        return result;
    }
}
