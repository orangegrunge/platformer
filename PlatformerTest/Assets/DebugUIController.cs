using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugUIController : MonoBehaviour
{
    public Transform livesHolder;
    public GameObject life;
    List<GameObject> livesList = new List<GameObject>();

    public TextMeshProUGUI timer;


    public void InitLives(int lives)
    {
        livesList.Add(life);
        for (int i = 1; i < lives; i++)
        {
            livesList.Add(Instantiate(life, livesHolder));
        }
    }

    public void LoseLife()
    {
        if (livesList.Count >= 1)
        {
            Destroy(livesList[livesList.Count - 1]);
            livesList = livesList.GetRange(0, livesList.Count - 1);
        }
    }

    internal void InitTimer(float timeLeft)
    {
        timer.text = timeLeft.ToString();
    }

    public void UpdateTimer(float timeLeft)
    {
        timer.text = Mathf.Floor(Mathf.Clamp(timeLeft, 0, 9999999999)).ToString();
    }
}
