using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class SceneMusic
{
    public string sceneName;
    public AudioClip music;

    

}


public class SessionManager : MonoBehaviour
{
    public List<SceneMusic> sceneMusic;
    public AudioSource audioSource;
    public Animator fader;
    [HideInInspector] public bool fade;
    [HideInInspector] int currenSceneTry;
    private bool unfade;
    public float firstSceneDuration;
    private int nextSceneIndex;
    public GameObject firstSceneUI;
    public bool newScene;
    internal int curentSceneIndex;

    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("SessionManager");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

    }

    public void Start()
    {
        //StartCoroutine(FirstSceneTimer());
    }

    IEnumerator FirstSceneTimer()
    {
        yield return new WaitForSeconds(firstSceneDuration);
        StartGame();
    }
    bool started;
    public bool fadingInProcess;

    private void StartGame()
    {
        started = true;
        firstSceneUI.SetActive(false);
        LoadNextScene();
    }

    public void Update()
    {
        if (!started && SceneManager.GetActiveScene().buildIndex == 0 && Input.GetKeyDown(KeyCode.X))
        {
            StartGame();
        }

        if (fade && fader.GetCurrentAnimatorStateInfo(0).IsName("HiddenIdle"))
        {
            fade = false;
            SceneManager.LoadScene(nextSceneIndex);
        }
        else if (unfade && fader.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            unfade = false;
            fadingInProcess = false;
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                firstSceneUI.SetActive(true);
            }
        }
    }

    
    public void LoadNextScene()
    {
        fadingInProcess = true;
        nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        FadeAndLoad(nextSceneIndex);
        newScene = true;
    }

    public void Reload()
    {
        nextSceneIndex = SceneManager.GetActiveScene().buildIndex;
        FadeAndLoad(nextSceneIndex);
        newScene = false;
    }

    void FadeAndLoad(int index)
    {
        Debug.Log("Fade");
        fade = true;
        fader.SetTrigger("fade");
    }

    public void FadeOut()
    {
        curentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        Debug.Log("Unfade");
        unfade = true;
        fader.SetTrigger("unfade");
    }

    public void StartAgain()
    {
        FadeAndLoad(0);
    }

    internal void PlayMusic()
    {
        audioSource.Play();
    }
}
