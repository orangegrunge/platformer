using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonActiveZone : MonoBehaviour
{
    SkeletonTrigger skeletonTrigger;

    private void Start()
    {
        skeletonTrigger = GetComponentInParent<SkeletonTrigger>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            skeletonTrigger.playerIsNear = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            skeletonTrigger.playerIsNear = false;
        }
    }
}
