using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogStarterTrigger : MonoBehaviour
{
    public string triggerTag;
    public string endSpeechTag;
    public float delay;
    private DialogController dialogController;

    void Start()
    {
        if (FindObjectOfType<SessionManager>().newScene) StartCoroutine(StartLevelDialog());
        dialogController = FindObjectOfType<DialogController>();
        dialogController.currentStarter = triggerTag;

    }

    IEnumerator StartLevelDialog()
    {
        yield return new WaitForSeconds(delay);
        dialogController.ShowSpeechStarter(triggerTag);
    }

    internal void EndLevel()
    {
        dialogController.ShowSpeechEnd(endSpeechTag);
    }
}
