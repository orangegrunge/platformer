using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    [Header("Refs")]
    public Collider2D collider;

    Vector2 startPoint;
    float amplitude;
    float period;
    float speed;
    float phase;

    float timer = 0;
    float deathZone = 30f;

    LevelStateManager stateManager;
    PlayerController player;
    Steer2D.SteeringAgent steeringAgent;
    bool isSteering = false;
    bool hold;

    public SpriteRenderer spriteRenderer;
    public bool inverted;

    public void Start()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
        player = FindObjectOfType<PlayerController>();
        if (steeringAgent)
        {
            steeringAgent.MaxVelocity = Mathf.Abs(speed);
            GetComponent<Steer2D.Pursue>().TargetAgent = player.steeringAgent;
        }
    }


    public void Init(Vector2 start, float ampl, float prd, float spd, float ph)
    {
        transform.position = start;
        startPoint = start;
        amplitude = ampl;
        period = prd;
        speed = spd;
        phase = ph;
        steeringAgent = GetComponent<Steer2D.SteeringAgent>();
        if (steeringAgent) isSteering = true;
    }

    internal void Stop()
    {
        hold = true;
    }

    void Update()
    {
        if (isSteering)
        {
            if (!stateManager.pause && !hold)
            {
                steeringAgent.enabled = true;

            }
            else steeringAgent.enabled = false;
            spriteRenderer.flipX = inverted ? steeringAgent.CurrentVelocity.x >= 0 : steeringAgent.CurrentVelocity.x < 0;
        }
        else
        {
            spriteRenderer.flipX = inverted ? speed >= 0 : speed < 0;
            if (!stateManager.pause && !hold)
            {
                timer += Time.deltaTime;

                float x = timer * speed;
                transform.position = startPoint + new Vector2(x, Mathf.Sin(2 * x * Mathf.PI / period + phase * 2 * Mathf.PI) * amplitude / 2);
            }
            else
            {

            }
        }
        
        if (Mathf.Abs(transform.position.x) > deathZone) Destroy(gameObject);
    }
}
