using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using System.IO;


[System.Serializable]
public class SpawnPreset
{
    public string triggerTag;

    [Header("Projectile Settings")]
    public string projectileName;
    public float speed;

    [Header("Path Settings")]
    public float amplitude;
    public float period;
    public float phase;

    [Header("Burst Settings")]
    public Vector2Int count;
    public Vector2 step;
    [Range(0,1)]
    public float offset;
    public Vector2 phaseOffset;

    public SpawnPreset()
    {

    }

    public SpawnPreset(string projectileName, float speed, float amplitude, float period, Vector2Int count, Vector2 step, float offset)
    {
        this.projectileName = projectileName;
        this.speed = speed;
        this.amplitude = amplitude;
        this.period = period;
        this.count = count;
        this.step = step;
        this.offset = offset;
    }

    public SpawnPreset(string projectileName, float speed, float amplitude, float period)
    {
        this.projectileName = projectileName;
        this.speed = speed;
        this.amplitude = amplitude;
        this.period = period;
        this.count = new Vector2Int(1,1);
        this.step = new Vector2(0,0);
        this.offset = 0;
    }
}

public enum SpawnSide
{
    LEFT, RIGHT
}

[System.Serializable]
public class SpawnAction
{
    public float triggerTime;
    public SpawnPreset spawnPreset;
    public SpawnSide spawnSide;
    public float spawnHeight;
    public float spawnXOffset;
    public bool seek;

    public SpawnAction(float triggerTime, SpawnPreset spawnPreset, SpawnSide spawnSide, float spawnHeight)
    {
        this.triggerTime = triggerTime;
        this.spawnPreset = spawnPreset;
        this.spawnSide = spawnSide;
        this.spawnHeight = spawnHeight;
        this.spawnXOffset = 0;
        this.seek = false;
    }

    public SpawnAction(float triggerTime, SpawnPreset spawnPreset, SpawnSide spawnSide)
    {
        this.triggerTime = triggerTime;
        this.spawnPreset = spawnPreset;
        this.spawnSide = spawnSide;
        this.spawnHeight = 0;
        this.spawnXOffset = 0;
        this.seek = true;
    }

    public SpawnAction(float triggerTime, string triggerTag, SpawnSide spawnSide, float spawnHeight)
    {
        this.triggerTime = triggerTime;
        this.spawnPreset = SpawnAction.GetSpawnPreset(triggerTag);
        this.spawnSide = spawnSide;
        this.spawnHeight = spawnHeight;
        this.spawnXOffset = 0;
        this.seek = false;
    }

    public SpawnAction(float triggerTime, string triggerTag, SpawnSide spawnSide, float spawnHeight, float spawnXOffset)
    {
        this.triggerTime = triggerTime;
        this.spawnPreset = SpawnAction.GetSpawnPreset(triggerTag);
        this.spawnSide = spawnSide;
        this.spawnHeight = spawnHeight;
        this.spawnXOffset = spawnXOffset;
        this.seek = false;
    }

    public SpawnAction(float triggerTime, string triggerTag, SpawnSide spawnSide)
    {
        this.triggerTime = triggerTime;
        this.spawnPreset = SpawnAction.GetSpawnPreset(triggerTag);
        this.spawnSide = spawnSide;
        this.spawnHeight = 0;
        this.spawnXOffset = 0;
        this.seek = true;
    }

    static SpawnPreset GetSpawnPreset(string tag)
    {
        if (presets == null) InitByJson();

        var preset = presets.Find(x => x.triggerTag == tag);
        return preset;
    }

    public static void InitByJson()
    {
        presets = LoadFromJson();
    }

    public static List<SpawnPreset> LoadFromJson()
    {
        TextAsset textAsset = Resources.Load<TextAsset>("configs/spawn_presets");
        string json = textAsset.text;
        return JsonUtility.FromJson<PresetListWrapper>(json).src;
    }

    static List<SpawnPreset> presets; 
 }

[System.Serializable]
public class PresetListWrapper {
    public List<SpawnPreset> src;

    public PresetListWrapper(List<SpawnPreset> list)
    {
        src = list;
    }
}

public class SpawnPresets
{
    //// PRESETS
    public SpawnPreset slowSkull = new SpawnPreset("skull", 1f, 1f, 3f);

    //// RANDOM PRESET GETTERS
    public SpawnPreset SkullBurstLine(int xMin, int xMax)
    {
        return new SpawnPreset("skull", 1f, 1f, 3f,
            new Vector2Int(Random.Range(xMin, xMax), 1),
            Vector2.one, 0);
    }
}

public class SpawnerController : MonoBehaviour
{
    public UnityEvent scenario;

    [Header("DEBUG")]
    public SpawnAction spawnActionDebug;


    List<SpawnAction> spawnActions = new List<SpawnAction>();
    LevelStateManager stateManager;
    PlayerController playerController;
    SpawnPresets SP;
    public float spawnMargin = -10f;

    ////////////////////////////////////
    // ��������

    public void JsonTestScenario()
    {
        spawnActions = new List<SpawnAction>(){

            new SpawnAction(1, "test", SpawnSide.LEFT),

        };
    }

    public void JsonLevel1Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(5, "SingleWave", SpawnSide.LEFT, -8),
            new SpawnAction(10, "Wall1x3", SpawnSide.RIGHT, -10),
            new SpawnAction(15, "Jaw2x2", SpawnSide.LEFT),
            new SpawnAction(20, "Jaw2x2", SpawnSide.RIGHT),
            new SpawnAction(25, "Wall1x3", SpawnSide.LEFT, -10),


        };
    }

    public void JsonLevel2Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(3, "SingleWave", SpawnSide.LEFT, -8),
            new SpawnAction(7, "Wall1x3", SpawnSide.RIGHT, -10),
            new SpawnAction(11, "Jaw2x2", SpawnSide.LEFT),
            new SpawnAction(15, "Snake7x1", SpawnSide.RIGHT, -8),
            new SpawnAction(19, "Wall1x3", SpawnSide.RIGHT),
            new SpawnAction(23, "Jaw2x2", SpawnSide.RIGHT),
            new SpawnAction(27, "Wall1x3", SpawnSide.LEFT, -10),


        };
    }

    public void JsonLevel3Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(2, "SingleWave", SpawnSide.LEFT, -3),
            new SpawnAction(7, "Wall1x3", SpawnSide.RIGHT, -3),
            new SpawnAction(11, "Jaw2x2", SpawnSide.LEFT),
            new SpawnAction(15, "Snake7x1", SpawnSide.RIGHT, -1),
            new SpawnAction(19, "Wall1x3", SpawnSide.RIGHT),
            new SpawnAction(23, "Jaw2x2", SpawnSide.RIGHT),
            new SpawnAction(27, "Wall1x3", SpawnSide.LEFT, -3),


        };
    }

    public void JsonLevel4Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(2, "Bat", SpawnSide.LEFT),
            new SpawnAction(7, "Wall1x3", SpawnSide.RIGHT, -3),
            new SpawnAction(11, "Jaw2x2", SpawnSide.LEFT),
            new SpawnAction(15.5f, "Bat", SpawnSide.RIGHT, - 5),
            new SpawnAction(19, "Wall1x3", SpawnSide.RIGHT),
            new SpawnAction(22, "Jaw2x2", SpawnSide.LEFT),
            new SpawnAction(26, "Cloud3x3", SpawnSide.RIGHT, 2),


        };
    }

    public void JsonLevel5Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(4, "CrossY", SpawnSide.RIGHT, 0, 1),
            new SpawnAction(4, "CrossX", SpawnSide.RIGHT, -1),
            new SpawnAction(4, "CrossY", SpawnSide.RIGHT, -2, 1),

            new SpawnAction(10, "CrossY", SpawnSide.LEFT, -4, -1),
            new SpawnAction(10, "CrossX", SpawnSide.LEFT, -5),
            new SpawnAction(10, "CrossY", SpawnSide.LEFT, -6, -1),

            new SpawnAction(15, "CrossY", SpawnSide.RIGHT, 4, 1),
            new SpawnAction(15, "CrossX", SpawnSide.RIGHT, 3),
            new SpawnAction(15, "CrossY", SpawnSide.RIGHT, 2, 1),

            new SpawnAction(20, "Bat", SpawnSide.LEFT, -5),


        };
    }

    public void JsonLevel6Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(4, "Jaw2x2", SpawnSide.RIGHT),
            new SpawnAction(7, "Cloud3x3", SpawnSide.LEFT, -10),
            new SpawnAction(10, "Snake7x1", SpawnSide.RIGHT, 2),
            new SpawnAction(15, "Wall1x15", SpawnSide.LEFT, -5),
            new SpawnAction(20, "Bat", SpawnSide.RIGHT),
            new SpawnAction(22, "Cloud3x3", SpawnSide.LEFT, 0),
            new SpawnAction(25, "Snake7x1", SpawnSide.RIGHT, -9),
            new SpawnAction(27, "Jaw5x2", SpawnSide.RIGHT),
        };
    }

    public void JsonLevel7Scenario()
    {
        spawnActions = new List<SpawnAction>(){
            new SpawnAction(1, "Row1x3", SpawnSide.LEFT, 0),
            new SpawnAction(4, "Jaw2x2", SpawnSide.RIGHT),
            new SpawnAction(7, "Cloud3x3", SpawnSide.LEFT, -10),
            new SpawnAction(10, "Snake7x1", SpawnSide.RIGHT, 8),
            new SpawnAction(13, "Wall1x15", SpawnSide.RIGHT, -10),
            new SpawnAction(16, "Jaw2x2", SpawnSide.LEFT),
            new SpawnAction(21, "Row1x3", SpawnSide.RIGHT),
            //new SpawnAction(22, "Cloud3x3", SpawnSide.LEFT, 0),
            new SpawnAction(25, "Snake7x1", SpawnSide.RIGHT, -8),
            new SpawnAction(27, "Wall1x15", SpawnSide.LEFT, 0),
        };
    }

    public void JsonLevel8Scenario()
    {
        spawnActions = new List<SpawnAction>(){
        };
    }

    public void Scenario1()
    {
        spawnActions = new List<SpawnAction>(){

            new SpawnAction(1, SP.SkullBurstLine(8, 10), SpawnSide.LEFT),

            new SpawnAction(5, SP.slowSkull, SpawnSide.LEFT),

            new SpawnAction(10, SP.slowSkull, SpawnSide.RIGHT, -2),
            new SpawnAction(11, SP.slowSkull, SpawnSide.RIGHT, -2),
            new SpawnAction(12, SP.slowSkull, SpawnSide.RIGHT, -2),
            new SpawnAction(13, SP.slowSkull, SpawnSide.RIGHT, -2),

        };
    }

    ///////////////////////////////////// 
    //  ������

    [ContextMenu("SPAWN_DEBUG")]
    public void SpawnDebug()
    {
        Spawn(spawnActionDebug);
    }

    
    public void Spawn(SpawnAction spawnAction)
    {
        var prefab = Resources.Load<GameObject>("Proj/" + spawnAction.spawnPreset.projectileName);
        SpawnPreset spawnPreset = spawnAction.spawnPreset;

        for (int j = 0; j < spawnPreset.count.y; j++)
        {
            for (int i = 0; i < spawnPreset.count.x; i++)
            {
                var instance = Instantiate(prefab);
                var proj = instance.GetComponent<Projectile>();
                float sign = spawnAction.spawnSide == SpawnSide.LEFT ? 1 : -1;

                float x = spawnMargin * sign + spawnAction.spawnXOffset;
                float y = (spawnAction.seek && playerController) ? playerController.transform.position.y : spawnAction.spawnHeight;

                x = x - i * spawnPreset.step.x * sign - spawnPreset.step.x * j * sign * spawnPreset.offset;
                y = y + j * spawnPreset.step.y;

                float speed = sign * spawnPreset.speed;

                float ph = spawnPreset.phase + i * spawnPreset.phaseOffset.x + j * spawnPreset.phaseOffset.y;

                proj.Init(new Vector2(x, y),
                    spawnPreset.amplitude, spawnPreset.period, speed, ph);


            }
        }

        
    }
    
    public void OperateTriggers()
    {
        if (scenario == null) return;

        var newActions = new List<SpawnAction>();

        foreach (var action in spawnActions)
        {
            if (action.triggerTime < stateManager.GetTime())
            {
                Debug.Log("Spawn: " + JsonUtility.ToJson(action));
                Spawn(action);
            }
            else newActions.Add(action);
        }
        spawnActions = newActions;
    }

    public void Start()
    {
        SP = new SpawnPresets();
        stateManager = FindObjectOfType<LevelStateManager>();
        playerController = FindObjectOfType<PlayerController>();
        scenario.Invoke();
    }

    public void Update()
    {
        OperateTriggers();
    }
#if UNITY_EDITOR
    [ContextMenu("IMPORT")]
    public void Import()
    {
        List<SpawnPreset> presets;

        presets = SpawnAction.LoadFromJson();

        
        var presetIndex = presets.FindIndex(x => x.triggerTag == spawnActionDebug.spawnPreset.triggerTag);
        if (presetIndex != -1)
        {
            var preset = presets[presetIndex];
            string backup = JsonUtility.ToJson(preset, true);
            string current = JsonUtility.ToJson(spawnActionDebug.spawnPreset, true);
            if (backup != current) Debug.LogWarning("������ " + preset.triggerTag + " ��� �����������, ������ ��������:" + System.Environment.NewLine + backup);
            else Debug.Log("������ " + preset.triggerTag + " � ����������� ���������� ��� �������� �����");
            presets[presetIndex] = spawnActionDebug.spawnPreset;
        }
        else
        {
            presets.Add(spawnActionDebug.spawnPreset);
            Debug.Log("������ " + spawnActionDebug.spawnPreset.triggerTag + " ��� ��������");
        }

        string json = JsonUtility.ToJson(new PresetListWrapper(presets), true);


        File.WriteAllText("Assets/Resources/configs/spawn_presets.json", json);
        AssetDatabase.Refresh();
    }
#endif
}
