using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileSpawner : MonoBehaviour
{
    public float speed;
    public string projectileName;

    public void Spawn()
    {
        var prefab = Resources.Load<GameObject>("Proj/" + projectileName);
        var instance = Instantiate(prefab);
        var proj = instance.GetComponent<Projectile>();

        proj.Init(transform.position,
            0, 100, speed, 0);
    }
}
