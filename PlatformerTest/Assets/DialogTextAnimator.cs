using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogTextAnimator : MonoBehaviour
{
    public AudioSource audioSource;
    public TextMeshProUGUI text;
    public float symbolsPerSecond;
    public float fastForwardSPS;
    public float amplitude;
    public float dumpingTime;
    public AnimationCurve dumpingCurve;

    string targetText;
    int symbolIndex;
    float secondsPerSymbol;
    List<float> offsets;
    //[HideInInspector]
    public bool done;
    float counter;
    float timer;

    public void Start()
    {
       // if (!FindObjectOfType<SessionManager>().newScene) done = true;
    }

    public void Animate(string t, AudioClip sound)
    {
        targetText = t;
        text.text = "";
        symbolIndex = 0;
        done = false;
        offsets = new List<float>();
        timer = 0;
        counter = 0;
        secondsPerSymbol = 1 / symbolsPerSecond;
        audioSource.clip = sound;
        audioSource.Play();
    }

    private void Update()
    {
        if (!done) {

            counter += Time.deltaTime;

            if (offsets.Count < targetText.Length)
            {
                timer += Time.deltaTime;
                if (timer > secondsPerSymbol)
                {
                    offsets.Add(counter);
                    timer = 0;
                }
            }
            else
            {
                audioSource.Stop();
            }

            text.text = GetString();

            if (offsets.Count >= targetText.Length && offsets[offsets.Count - 1] + dumpingTime < counter)
            {
                done = true;
                audioSource.Stop();
                //text.text = targetText;
            }
        }
        
        
        
    }

    string GetString()
    {
        var s = "<line-height=100%>";
        for (int i = 0; i < offsets.Count; i++)
        {
            var x = offsets[i];
            var symbol = targetText[i];
            var position = GetPosition(x);
            s += string.Format("<voffset={0}>{1}", position, symbol);
        }
        s += "</voffset>";
        return s;
    }


    public float GetPosition(float offset)
    {
        return dumpingCurve.Evaluate(Mathf.Clamp((counter - offset) / dumpingTime, 0, 1)) * amplitude;
    }

    public void FastForward()
    {
        text.text = targetText;
        done = true;
        audioSource.Stop();
    }
}
