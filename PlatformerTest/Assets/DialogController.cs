using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DialogController : MonoBehaviour
{
    [Header("Settings")]
    public DialogPalette dialogPalette;
    public DialogScript dialogScript;
    [Header("Refs")]
    public Image panel;
    public Image faceFrame;
    public Image faceImage;
    public Image textFrame;
    public TextMeshProUGUI text;
    public DialogTextAnimator textAnimator;

    InputController inputController;

    DialogSpeech speech;
    int lineIndex = -1;
    [HideInInspector] public string currentStarter;
    BellManager bellManager;
    LevelStateManager stateManager;
    SessionManager sessionManager;
    private bool hasDialog;
    public bool win;
    private bool r;

    internal void ShowSpeechEnd(string endSpeechTag)
    {
        win = true;
        speech = dialogScript.GetSpeech(endSpeechTag);
        speech.nextSpeechTag = "_end";
        ShowSpeech(endSpeechTag);
    }

    public void Start()
    {
        inputController = FindObjectOfType<InputController>();
        bellManager = FindObjectOfType<BellManager>();
        stateManager = FindObjectOfType<LevelStateManager>();
        sessionManager = FindObjectOfType<SessionManager>();
    }

    public void ShowSpeech(string triggerTag)
    {
        inputController.Lock();
        bellManager.Pause();
        stateManager.SetPause(true);

        speech = dialogScript.GetSpeech(triggerTag);
        lineIndex = -1;
        panel.gameObject.SetActive(true);
        hasDialog = true;
        ShowLine();
    }

    internal void ShowSpeechStarter(string triggerTag)
    {
        currentStarter = triggerTag;
        ShowSpeech(triggerTag);
    }

    public void ShowLine()
    {
        lineIndex++;
        if (speech.lines.Count <= lineIndex)
        {
            if (speech.nextSpeechTag == "_starter")
            {
                ShowSpeech(currentStarter);
            }
            else if (speech.nextSpeechTag == "_end")
            {
                sessionManager.LoadNextScene();
            }
            else if (speech.nextSpeechTag != "")
            {
                ShowSpeech(speech.nextSpeechTag);
            }
            else
            {
                panel.gameObject.SetActive(false);
                hasDialog = false;
                inputController.Unlock();
                bellManager.Unpause();
                stateManager.SetPause(false);
                if (speech.triggerTag == currentStarter && sessionManager.curentSceneIndex == 1 && sessionManager.newScene)
                {
                    sessionManager.PlayMusic();
                } 

                if (speech.triggerTag == currentStarter && !bellManager.started)
                {
                    bellManager.StartSequence();
                } 
            }
        }
        else
        {
            SetPalette();
            DialogLine dialogLine = speech.lines[lineIndex];
            faceImage.sprite = dialogLine.face;
            textAnimator.Animate(dialogLine.text, dialogLine.voice);
        }
    }

    public void SetPalette()
    {
        var paletteTag = speech.lines[lineIndex].palette;
        var palette = dialogPalette.GetPalette(paletteTag);
        panel.color = palette.panel;
        faceFrame.color = palette.frameFace;
        faceImage.color = palette.imageFace;
        textFrame.color = palette.frameText;
        text.color = palette.text;
        text.fontSize = palette.textSize;
    }

    public void Update()
    {
        if (!sessionManager.fadingInProcess && hasDialog && speech != null && speech.triggerTag != "restart_text" && Input.GetKeyDown(KeyCode.X))
        {
            if (!textAnimator.done) textAnimator.FastForward();
            else ShowLine();
        }
        else if (!r && speech != null && speech.triggerTag == "restart_text" && Input.GetKeyDown(KeyCode.R))
        {
            r = true;
            sessionManager.Reload();
        }
    }
}
