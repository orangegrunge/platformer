using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

[ExecuteInEditMode]
public class PathController : MonoBehaviour
{
    [System.Serializable]
    public class PathSettings
    {
        public PathLogic pathLogic;
        public bool initMoving;
        public bool straightDirection;

        public bool m_showSpeedSettings;
        public float speed;
        public AnimationCurve speedCurve;

        public bool m_showStartPositionSettings;
        public int segment;
        [Range(0, 1)]
        public float segmentPart;
        public int segmentsCount;
    }

    public enum PathLogic {
        CYCLE,
        PATROL,
        ENDPOINT
    }

    private bool isMoving;
    private float speed;
    private bool straightDirection;
    private int wpIndex;
    public PathSettings settings;
    private List<Transform> waypoints;
    private List<Vector3> wpVectors;
    BoxCollider2D boxCollider;

    private int targetWpIndex;
    private int previousWpIndex;

    private Vector3 targetVector;
    private Vector3 previousVector;
    private Vector3 dirSegment;
    private float segmentLength;

    private void Start()
    {
        if (Application.isPlaying)
        {
            isMoving = settings.initMoving;
            speed = settings.speed;
            straightDirection = settings.straightDirection;
            wpIndex = settings.segment;

            waypoints = new List<Transform>();
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                if (child.tag == "WayPoint") waypoints.Add(child);
            }
            wpVectors = waypoints.FindAll(x => x).Select(x => x.position).ToList();
            waypoints.ForEach(x => Destroy(x.gameObject));

            Vector3 pos = wpVectors[wpIndex];
            bool isLast = settings.segment == wpVectors.Count - 1;
            var next = isLast ? wpVectors[0] : wpVectors[settings.segment + 1];
            transform.position = pos + settings.segmentPart * (next - pos);

            SetTargetIndex();
            CalculateSegment();
        }
        
    }

    [ContextMenu("SwitchDirection")]
    public void SwitchDirection()
    {
        straightDirection = !straightDirection;
        SetNextTargets();
    }

    public void SetDirection(bool isStraight)
    {
        straightDirection = isStraight;
    }

    public void SwitchMoving()
    {
        isMoving = !isMoving;
    }

    public void SetMoving(bool enabled)
    {
        isMoving = enabled;
    }

    public void SetSpeed(float spd)
    {
        speed = spd;
    }

    private void OnEnable()
    {
        if (!Application.isPlaying) waypoints = new List<Transform>();
    }

    private void Update()
    {
        if (!Application.isPlaying)
        {
#if UNITY_EDITOR
            var childPoints = new List<Transform>();
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                if (child.tag == "WayPoint") childPoints.Add(child);
            }
            if (!Enumerable.SequenceEqual(childPoints, waypoints))
            {
                waypoints = childPoints;
            }
            wpVectors = waypoints.FindAll(x => x).Select(x => x.position).ToList();

            settings.segmentsCount = settings.pathLogic == PathLogic.CYCLE
                ? wpVectors.Count - 1
                : wpVectors.Count - 2;
            settings.segment = Mathf.Clamp(settings.segment, 0, settings.segmentsCount);
#endif
        }
        else
        {
            if (isMoving)
            {
                float lengthTraveled = (transform.position - previousVector).magnitude;
                float lengthRest = segmentLength - lengthTraveled;
                float segmentPart = lengthTraveled / segmentLength;
                float curSpeed = speed * Time.deltaTime * settings.speedCurve.Evaluate(segmentPart);
                if (curSpeed > lengthRest)
                {
                    transform.Translate(lengthRest * dirSegment.normalized);
                    SetNextTargets();
                }
                else
                {
                    transform.Translate(curSpeed * dirSegment.normalized);
                }
                
            }
        }
        
    }

    private void SetTargetIndex()
    {
        targetWpIndex = straightDirection ? wpIndex + 1 : wpIndex;
        previousWpIndex = straightDirection ? wpIndex : wpIndex + 1;
        
    }

    private void SetNextTargets()
    {
        int newTargetWpIndex = straightDirection ? targetWpIndex + 1 : targetWpIndex - 1;
        int newPreviousWpIndex = straightDirection ? newTargetWpIndex - 1 : newTargetWpIndex + 1;


        if (settings.pathLogic == PathLogic.ENDPOINT)
        {
            if (newTargetWpIndex > wpVectors.Count - 1 || newTargetWpIndex < 0) {}
            else
            {
                targetWpIndex = newTargetWpIndex;
                previousWpIndex = newPreviousWpIndex;
            }
        }
        else if (settings.pathLogic == PathLogic.PATROL)
        {
            if (newTargetWpIndex > wpVectors.Count - 1)
            {
                straightDirection = !straightDirection;
                targetWpIndex = newPreviousWpIndex - 1;
                previousWpIndex = newTargetWpIndex - 1;
            }
            else if (newTargetWpIndex < 0)
            {
                straightDirection = !straightDirection;
                targetWpIndex = newPreviousWpIndex + 1;
                previousWpIndex = newTargetWpIndex + 1;
            }
            else
            {
                targetWpIndex = newTargetWpIndex;
                previousWpIndex = newPreviousWpIndex;
            }
        }
        else if (settings.pathLogic == PathLogic.CYCLE)
        {
            if (newTargetWpIndex > wpVectors.Count - 1)
            {
                targetWpIndex = 0;
                previousWpIndex = newPreviousWpIndex;
            }
            else if (newTargetWpIndex < 0)
            {
                targetWpIndex = wpVectors.Count - 1;
                previousWpIndex = 0;
            }
            else
            {
                targetWpIndex = newTargetWpIndex;
                previousWpIndex = newPreviousWpIndex;
            }
        }
        
        CalculateSegment();
    }

    private void CalculateSegment()
    {
        targetVector = wpVectors[targetWpIndex];
        previousVector = wpVectors[previousWpIndex];
        dirSegment = targetVector - previousVector;
        segmentLength = dirSegment.magnitude;
    }


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        DrawWaypointPath();
        DrawWaypointsBounds();
        DrawDirection();
    }

    private void DrawWaypointPath()
    {
        for (int i = 1; i < wpVectors.Count; i++)
        {
            DrawWay(wpVectors[i - 1], wpVectors[i]);
        }
        if (settings.pathLogic == PathLogic.CYCLE)
        {
            DrawWay(wpVectors[0], wpVectors[wpVectors.Count - 1]);
        }
    }

    private void DrawWaypointsBounds()
    {
        for (int i = 0; i < wpVectors.Count; i++)
        {
            Vector3 pos = wpVectors[i];
            DrawBounds(pos, Color.yellow);
            if (settings.segment == i)
            {
                bool isLast = i == wpVectors.Count - 1;
                if (settings.pathLogic != PathLogic.CYCLE && isLast) continue;
                else
                {
                    var next = isLast ? wpVectors[0] : wpVectors[i+1];
                    DrawBounds(pos + settings.segmentPart * (next - pos), Color.green);
                }
            }
        }
    }

    private void DrawBounds(Vector3 pos1,  Color color)
    {
        List<Vector3> localCorners = GetLocalCorners();
        for (int i = 0; i < localCorners.Count; i++)
        {
            Vector3 corner = localCorners[i];
            Vector3 prevCorner = i == 0 ? localCorners[localCorners.Count - 1] : localCorners[i - 1];
            Debug.DrawLine(pos1 + corner, pos1 + prevCorner, color);
        }
    }

    private void DrawWay(Vector3 pos1, Vector3 pos2)
    {
        boxCollider = GetComponent<BoxCollider2D>();
        if (boxCollider)
        {
            List<Vector3> localCorners = GetLocalCorners();

            for (int i = 0; i < localCorners.Count; i++)
            {
                Vector3 corner = localCorners[i];
                Debug.DrawLine(pos1 + corner, pos2 + corner, new Color(0.7f, 0.7f, 0.7f));
            }
        }
    }

    private List<Vector3> GetLocalCorners()
    {
        Vector2 halfSize = boxCollider.bounds.extents;
        List<Vector3> localCorners = new List<Vector3>()
            {
                new Vector3(halfSize.x, halfSize.y),
                new Vector3(-halfSize.x, halfSize.y),
                new Vector3(-halfSize.x, -halfSize.y),
                new Vector3(halfSize.x, -halfSize.y)
            };
        return localCorners;
    }

    private void DrawDirection()
    {
        Vector3 pos = wpVectors[settings.segment];
        bool isLast = settings.segment == wpVectors.Count - 1;
        var next = isLast ? wpVectors[0] : wpVectors[settings.segment + 1];
        Vector3 center = pos + settings.segmentPart * (next - pos);
        Vector2 forwardDir = next - pos;
        List<Vector3> points = GetDirectionPoints(forwardDir);
        for (int i = 0; i < points.Count; i++)
        {
            Vector3 corner = points[i];
            Vector3 prevCorner = i == 0 ? points[points.Count - 1] : points[i - 1];
            Debug.DrawLine(center + corner, center + prevCorner, Color.green);
        }

    }

    private static List<Vector3> GetDirectionPoints(Vector2 forwardDir)
    {
        Vector2 x = forwardDir.normalized;
        Vector2 y = Vector2.Perpendicular(x);

        List<Vector3> points = new List<Vector3>()
        {
            0.1f * x + 0.2f * y,
            0.3f * x + 0f * y,
            0.1f * x - 0.2f * y
        };
        return points;
    }
#endif
}
