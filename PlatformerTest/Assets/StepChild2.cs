using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System;

[ExecuteInEditMode]
public class StepChild2 : MonoBehaviour
{
    GameSettings gameSettings;
    Vector3Int vInt;
    List<string> possibleOffsets;
    Vector3 v;
    public Switch mainObjectSwitch;
    [HideInInspector]
    public Vector3 offset;

    Dictionary<string, Vector3> offsets = new Dictionary<string, Vector3>()
    {
        {"left",  Vector3Int.left},
        {"up", Vector3Int.up},
        {"right",  Vector3Int.right},
        {"down",  Vector3Int.down},
    };

    Dictionary<string, float> angles = new Dictionary<string, float>()
    {
        {"left",  90},
        {"up", 0},
        {"right",  270},
        {"down",  180},
    };
    Vector3 o;
    string dir;
    private bool targetIsPlatform;
    private bool stickToPlatform;
    private GameObject target;
    private GameObject parent;

    public GameObject Parent { get => parent; set => parent = value; }

    private void Start()
    {
        RoundPosition();
        if (parent) offset = transform.position - parent.transform.position;
    }



    private void Update()
    {
        if (!Application.isPlaying) RoundPosition();
        else
        {
            if (transform.parent && transform.parent.gameObject == parent) return;
            else if (parent) transform.position = parent.transform.position + offset;
        }
    }


    private bool CheckTiles()
    {
        List<string> result = new List<string>();
        List<RaycastHit2D> hits = new List<RaycastHit2D>(Physics2D.RaycastAll((Vector3)vInt - Vector3.forward, Vector3.forward, 1));
        hits = hits.FindAll(x => x.collider.tag == "Level");
        if (hits.Count > 0)
        {
            Tilemap tilemap = ((TilemapCollider2D)hits[0].collider).GetComponent<Tilemap>();
            TileBase tile = tilemap.GetTile(tilemap.WorldToCell(vInt));
            if (tile)
            {
                foreach (string key in offsets.Keys)
                {
                    Vector3 origin = vInt + offsets[key];
                    TileBase adjacent = tilemap.GetTile(tilemap.WorldToCell(origin));
                    if (!adjacent)
                    {
                        //Debug.DrawLine(origin, vInt, Color.green);
                        result.Add(key);
                    }
                    else
                    {
                        //Debug.DrawLine(origin, vInt, Color.red);
                    }
                }
            }
        }
        possibleOffsets = result;
        return possibleOffsets.Count > 0;
    }

    private void RoundPosition()
    {
        v = transform.position;
        vInt = Vector3Int.RoundToInt((Vector2)v);


        if (!CheckTiles())
        {
            CheckPlatforms();
        }
        StickToSide();
        AddParents();
    }

    private void AddParents()
    {
        if (target)
        {
            parent = target;
            mainObjectSwitch.SetTriggersByStepChild(this);
            
        }
        else if (parent)
        {
            mainObjectSwitch.SetTriggersByStepChild(null);
            parent = null;
        }
        else
        {
            parent = null;
        }
    }

    private void CheckPlatforms()
    {
        List<string> result = new List<string>();
        List<RaycastHit2D> hits = new List<RaycastHit2D>(Physics2D.RaycastAll((Vector3)vInt - Vector3.forward, Vector3.forward, 1));
        hits = hits.FindAll(FilterPlatforms());
        if (hits.Count > 0)
        {
            foreach (string key in offsets.Keys)
            {
                Vector3 origin = (Vector3)vInt + offsets[key];
                List<RaycastHit2D> adjacent = new List<RaycastHit2D>(Physics2D.RaycastAll(origin - Vector3.forward, Vector3.forward, 1));
                adjacent = adjacent.FindAll(FilterPlatforms());
                if (adjacent.Count == 0)
                {
                    //Debug.DrawLine(origin, vInt, Color.green);
                    result.Add(key);
                }
                else
                {
                    //Debug.DrawLine(origin, vInt, Color.red);
                }
            }
        }
        possibleOffsets = result;

        if (possibleOffsets.Count > 0) target = hits[0].collider.gameObject;
        else target = null;
    }

    private Predicate<RaycastHit2D> FilterPlatforms()
    {
        return x => x.collider.gameObject != gameObject && x.collider.gameObject.layer == 6;
    }

    private void StickToSide()
    {
        o = v - vInt;
        if (o.x > 0)
        {
            if (o.y > 0)
            {
                dir = (o.x > o.y) ? "right" : "up";
            }
            else
            {
                dir = (o.x > -o.y) ? "right" : "down";
            }
        }
        else
        {
            if (o.y > 0)
            {
                dir = (-o.x > o.y) ? "left" : "up";
            }
            else
            {
                dir = (-o.x > -o.y) ? "left" : "down";
            }
        }
        stickToPlatform = false;
        if (possibleOffsets.Contains(dir))
        {
            o = offsets[dir] * 0.25f;

            EditorUtility.DrawIcon(gameObject, 3);

            mainObjectSwitch.transform.eulerAngles = new Vector3(0, 0, angles[dir]);
            mainObjectSwitch.transform.localPosition = offsets[dir] * 0.75f;
        }
        else
        {

            EditorUtility.DrawIcon(gameObject, 6);
        }

        transform.position = vInt + o;
    }

}
