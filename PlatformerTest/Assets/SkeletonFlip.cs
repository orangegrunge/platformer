using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonFlip : MonoBehaviour
{
    public BellHorizontalController horizontalController;
    public SpriteRenderer spriteRenderer;

    private void Update()
    {
        if (spriteRenderer && horizontalController) spriteRenderer.flipX = !horizontalController.goRight;

    }
}
