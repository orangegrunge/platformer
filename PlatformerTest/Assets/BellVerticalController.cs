using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class BellVerticalController : MonoBehaviour
{

    [Header("Refs")]
    public Transform ropeTransform;
    public SpriteRenderer ropeSprite;
    public Transform bell;
    public Transform topPoint;
    public Transform bottomPoint;
    [Header("Settings")]
    public float speed;
    public bool goDown;

    LevelStateManager stateManager;

    private void Start()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
    }

    private void Update()
    {
        

        if (!Application.isPlaying)
        {
            topPoint.localPosition = new Vector2(0, Mathf.Clamp(topPoint.localPosition.y, bottomPoint.localPosition.y, 0));
            bottomPoint.localPosition = new Vector2(0, bottomPoint.localPosition.y);
            SetBellPosition();
        }
        else
        {
            MoveBell();
        }
        SetRopePosition();
    }

    private void MoveBell()
    {
        if (stateManager.pause) return;
        
        if (goDown)
        {
            bell.transform.Translate(Vector2.down * speed * Time.deltaTime);
            if (bell.transform.position.y <= bottomPoint.position.y)
            {
                goDown = false;
            }
        }
        else
        {
            bell.transform.Translate(Vector2.up * speed * Time.deltaTime);
            if (bell.transform.position.y >= topPoint.position.y)
            {
                goDown = true;
            }
        }
    }

    private void SetBellPosition()
    {
        bell.transform.localPosition = new Vector2(0, Mathf.Clamp(bell.transform.localPosition.y, bottomPoint.localPosition.y, topPoint.localPosition.y));
    }

    private void SetRopePosition()
    {
        ropeTransform.localPosition = bell.transform.localPosition;
        ropeSprite.size = new Vector2(1, (transform.position - ropeTransform.position).magnitude);
    }
}
