using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Box : MonoBehaviour
{

    [Header("Settings")]
    public float moveSpeedMultiplier;
    public float jumpSpeedMultiplier;
    public float dashSpeedMultiplier;
    public float onSpikesSlidingSpeed;

    float gravity;
    float maxJumpVelocity;
    Vector3 velocity;
    bool queuedJump;
    int currentJumpCharges = 1;
    bool dashing;
    int dashDirection = 1;
    int currentDashCharges;
    float dashSpeed;

    Controller2D controller;
    PlayerController player;

    [Header("Refs")]
    public SpriteRenderer spriteRenderer;
    

    Vector2 directionalInput = new Vector2();
    private bool slide;
    private Vector3 slideTarget;

    void Start()
    {
        controller = GetComponent<Controller2D>();
        player = FindObjectOfType<PlayerController>();
    }

    void Init()
    {

    }

    void Update()
    {

        Move();
        SlideOnSpikes();
    }

    private void SlideOnSpikes()
    {
        if (slide)
        {
           
            transform.position = Vector2.Lerp(transform.position, slideTarget, 0.5f * Time.deltaTime * onSpikesSlidingSpeed);
            if ((transform.position - slideTarget).sqrMagnitude < 0.01f)
            {
                transform.position = slideTarget;
                slide = false;
            }
        }
    }

    private void Move()
    {
        if (!controller) return;

        CalculateVelocity();

        controller.Move(velocity * Time.deltaTime, directionalInput);

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }
    }

    internal void SetDirectionalInput(Vector2 input)
    {
        //Debug.Log("Box directional input: " + input);
        directionalInput = input;
    }

    void CalculateVelocity()
    {
        gravity = player.gravity;
        maxJumpVelocity = player.maxJumpVelocity;

        float m = moveSpeedMultiplier;
        if (player.dashing) {
            m = dashSpeedMultiplier;
        }
        else if (!player.controller.collisions.below) m = jumpSpeedMultiplier;

        float targetVelocityX = 
            directionalInput.x
            * m 
            * (player.dashing ? 1 : player.moveSpeed);
        velocity.x = targetVelocityX;
        velocity.y += gravity * Time.deltaTime;
        velocity.y = Mathf.Clamp(velocity.y, -maxJumpVelocity, maxJumpVelocity);
        directionalInput = Vector2.zero;
    }

    public Vector2 GetPossibleMoveAmount(Vector2 amount)
    {
        float m = moveSpeedMultiplier;
        if (player.dashing) m = dashSpeedMultiplier;
        else if (!player.controller.collisions.below) m = jumpSpeedMultiplier;


        return controller.GetPossibleMoveAmount(amount * m);
    }

    public void SlideDown()
    {
        slide = true;
        Hazard hazard = GetComponent<HazardReaction>().hazard;
        slideTarget = hazard.transform.position;
        hazard.tag = "Untagged";
        tag = "Untagged";
        Destroy(controller);

    }

    public void BurnDown()
    {
        Destroy(gameObject);
    }
}
