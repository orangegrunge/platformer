using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BellManager : MonoBehaviour
{
    Dictionary<int, BellController> bellControllers = new Dictionary<int, BellController>();
    int nextIndex;
    public float tempo;
    public string sequence;
    //[HideInInspector]
    public bool started;
    //[HideInInspector]
    public bool pause;
    LevelStateManager stateManager;
    SessionManager sessionManager;
    public float winSequencePeriod;
    DialogController dialogController;

    List<EnemyExplode> enemies;
     public float winSequencePeriodExplode;

    public void Start()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
        sessionManager = FindObjectOfType<SessionManager>();
        dialogController = FindObjectOfType<DialogController>();
        if (!sessionManager.newScene) StartSequence(); 
    }

    public void StartSequence()
    {
        Debug.Log("Start bell sequence");
        nextIndex = 0;
        Ring();
        started = true;
    }

    public void Ring()
    {
        if (sequence.Length <= nextIndex)
        {
            Debug.LogWarning("BELL SEQUENCE FINISHED.START AGAIN.");
            nextIndex = 0;
        }
        int nextBellId = int.Parse(sequence[nextIndex].ToString());
        Debug.Log("Next bell is " + nextBellId.ToString());
        bellControllers[nextBellId].Activate();
        nextIndex++;
    }

    public void RegisterBell(BellController bellController) {
        bellControllers.Add(bellController.id, bellController);
    }

    internal void Pause()
    {
        pause = true;
    }

    internal void Unpause()
    {
        pause = false;
    }

    internal void Miss()
    {
        stateManager.Miss();
    }

    public void Stop()
    {
        foreach (var bell in bellControllers.Values) {
            
            bell.Deactivate();
        }
        enemies = new List<EnemyExplode>();
        List<GameObject> enemyObjects = new List<GameObject>(GameObject.FindGameObjectsWithTag("enemy"));
        enemyObjects.ForEach(x => enemies.Add(x.GetComponent<EnemyExplode>()));
        StartCoroutine(RingAndCallNext(0));
    }

    IEnumerator RingAndCallNext(int index)
    {
        bellControllers[index].DoSound();
        yield return new WaitForSeconds(winSequencePeriod);
        index++;
        if (index < bellControllers.Count) StartCoroutine(RingAndCallNext(index));
        else StartCoroutine(ExplodeEnemyAndCallNext(0));
    }

    IEnumerator ExplodeEnemyAndCallNext(int index)
    {
        if (enemies.Count > 0 && enemies[index]) enemies[index].Explode();
        yield return new WaitForSeconds(winSequencePeriodExplode);
        index++;
        if (index < enemies.Count) StartCoroutine(ExplodeEnemyAndCallNext(index));
        else FindObjectOfType<DialogStarterTrigger>().EndLevel();
    } 
}
