using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class HazardReactionEvent
{
    public Hazard.Tag tag;
    public Hazard.Side side;
    public UnityEvent reaction;
}

public class HazardReaction : MonoBehaviour
{
    //public List<HazardReactionEvent> reactions = new List<HazardReactionEvent>();
    public List<HazardReactionEvent> reactions = new List<HazardReactionEvent>();
    [HideInInspector] public Hazard hazard;

    internal void React(RaycastHit2D hazardHit)
    {

        BoxCollider2D hazardCollider = (BoxCollider2D)hazardHit.collider;
        hazard = hazardCollider.GetComponent<Hazard>();
        Hazard.Tag hazardTag = hazard.hazardTag;
        Vector2 point = hazardHit.point;
        Vector2 direction = point - (Vector2)hazardCollider.bounds.center;
        float angle = Vector2.Angle((Vector2)hazardCollider.transform.up, direction);
        Hazard.Side side;
        if (angle <= 45) side = Hazard.Side.TOP;
        else if (angle >= 135) side = Hazard.Side.BOTTOM;
        else side = Hazard.Side.SIDE;


        HazardReactionEvent fallbackReaction = null;
        foreach (HazardReactionEvent reaction in reactions)
        {
            if (reaction.tag == hazardTag)
            {
                if (reaction.side == Hazard.Side.ALL) fallbackReaction = reaction;
                else if (reaction.side == side) {
                    reaction.reaction.Invoke();
                    return;
                }
            }
        }

        if (fallbackReaction != null) fallbackReaction.reaction.Invoke();
    }
}
