using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TImeBomb : MonoBehaviour
{
    public float duration;


    void Start()
    {
        StartCoroutine(CleanUp());
    }

    IEnumerator CleanUp()
    {
        yield return new WaitForSeconds(duration);
        Destroy(gameObject);
    }
}
