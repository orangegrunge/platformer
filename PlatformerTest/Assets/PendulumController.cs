using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class PendulumController : MonoBehaviour
{
    [Header("Refs")]
    public Transform ball;
    public Transform movingPart;
    public Transform ropeTransform;
    public SpriteRenderer ropeSprite;
    public Transform angleMark;
    public Transform angleMark_1;


    [Header("Settings")]
    public float velocity;
    public bool fullCircle;
    public bool goClockwise;

    float timer;
    LevelStateManager stateManager;
    float radius;
    float halfAngle;
    float minusHalfAngle;
    float oppositeAngleMark;
    public float initialAngle;


    private void Start()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
        float z = movingPart.rotation.eulerAngles.z;
        initialAngle = z > 180 ? z - 360 : z;
        timer = 0;
        halfAngle = Vector2.SignedAngle(Vector3.down, angleMark.localPosition);
    }

    public void Update()
    {
        if (!Application.isPlaying)
        {
            radius = (transform.position - angleMark.position).magnitude;
            if (!fullCircle)
            {
                halfAngle = Vector2.SignedAngle(Vector3.down, angleMark.localPosition);
                minusHalfAngle = -halfAngle;
                angleMark_1.localPosition = new Vector2(-angleMark.localPosition.x, angleMark.localPosition.y);
            }
            float z = movingPart.rotation.eulerAngles.z;
            initialAngle = z > 180 ? z - 360 : z;
            if (!fullCircle)
            {
               movingPart.eulerAngles = new Vector3(0, 0, Mathf.Clamp(initialAngle,
               Mathf.Min(halfAngle, -halfAngle), Mathf.Max(halfAngle, -halfAngle)));
            }
            ball.localPosition = new Vector3(0, -radius, 0);
            SetRopePosition();
        }
        else
        {
            float sign = goClockwise ? 1 : -1;
            if (!stateManager.pause)
            {
                if (fullCircle)
                {
                    movingPart.Rotate(new Vector3(0, 0, Mathf.Rad2Deg * sign * 2 * Mathf.PI * velocity) * Time.deltaTime);
                }
                else
                {
                    timer += Time.deltaTime;
                    float phase = Mathf.Asin(initialAngle * Mathf.Deg2Rad);

                    Vector3 currentRotation = Vector3.forward * Mathf.Sin(sign * timer + phase) * halfAngle;
                    //Debug.LogWarning(currentRotation);
                    movingPart.eulerAngles = currentRotation;
                    
                }
            }
            

        }
    }

    private void SetRopePosition()
    {
        ropeTransform.localPosition = ball.transform.localPosition;
        ropeSprite.size = new Vector2(1, (transform.position - ropeTransform.position).magnitude);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (fullCircle) {
            Handles.DrawWireArc(transform.position, Vector3.forward, Vector3.left, 360, radius);
        }
        else
        {
            Handles.DrawWireArc(transform.position, Vector3.forward, Vector3.down, halfAngle, radius);
            Handles.DrawWireArc(transform.position, Vector3.forward, Vector3.down, -halfAngle, radius);
            Handles.DrawLine(transform.position, angleMark.position);
            Handles.DrawLine(transform.position, angleMark_1.position);
        }
    }
#endif
}
