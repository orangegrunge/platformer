using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class TriggerVolume : MonoBehaviour
{
    public UnityEvent onVolumeEnter;
    public UnityEvent onVolumeExit;

    HashSet<Collider2D> collisions = new HashSet<Collider2D>();

    private void Awake()
    {
        if (onVolumeEnter == null) onVolumeEnter = new UnityEvent();
        if (onVolumeExit == null) onVolumeExit = new UnityEvent();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "Box") {
            collisions.Add(collision);
        }
        if (collisions.Count == 1) onVolumeEnter.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "Box")
        {
            collisions.Remove(collision);
        }
        if (collisions.Count == 0) onVolumeExit.Invoke();
    }
}
