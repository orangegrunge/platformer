using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class ActionSpot : MonoBehaviour
{
    public Transform hintPosition;
    public UnityEvent onActionKeyDown;
    public bool active;

    private void Awake()
    {
        if (onActionKeyDown == null) onActionKeyDown = new UnityEvent();
    }

    public void OnActionKeyDown()
    {
        if (active)
        {
            onActionKeyDown.Invoke();
        }
        
    }
}
