using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ActionHint : MonoBehaviour
{
    public TextMeshProUGUI textLabel;
    string text;
    public RectTransform parent;
    RectTransform rectTransform;

    public void Start()
    {
        text = FindObjectOfType<InputController>().action.ToString();
        rectTransform = GetComponent<RectTransform>();
        //parent = transform.parent.GetComponent<RectTransform>();
    }

    public void Set(bool active, Vector3 worldPos)
    {
        
        gameObject.SetActive(active);
        if (active)
        {
            textLabel.text = text;
            var screenPos = Camera.main.WorldToScreenPoint(worldPos);
            
            Vector2 localPos = new Vector2();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parent, screenPos, null, out localPos);
            GetComponent<RectTransform>().anchoredPosition = localPos;
        }
    }
}
