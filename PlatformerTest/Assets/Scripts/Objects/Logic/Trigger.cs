using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;



[ExecuteInEditMode]
public class Trigger : MonoBehaviour
{
    public enum TimerMode
    {
        NONE,
        ACTIVATE,
        DEACTIVATE
    }

    [System.Serializable]
    public class TriggerSettings
    {
        public bool controlledBySwitch;
        public bool initActive;
        public TimerMode timerMode;
        public float time;

        public bool GetTargetState()
        {
            if (timerMode == TimerMode.ACTIVATE) return true;
            else return false;
        }
    }

    //[Header("Refs")]

    [HideInInspector]
    public bool isActivated;


    [Header("Trigger Settings")]
    public TriggerSettings settings;

    bool timerTargetState;
    float timeLeft;

    GameSettings gameSettings;
    UnityEvent stateUpdated;

    public void OnEnable()
    {
        if (stateUpdated == null) stateUpdated = new UnityEvent();
    }

    public void Start()
    {
        timerTargetState = settings.GetTargetState();
        gameSettings = FindObjectOfType<GameSettings>();
        if (!settings.controlledBySwitch) SetState(settings.initActive);
        
    }

    public void SwitchState()
    {
        SetState(!isActivated);
    }

    public void SetState(bool activate)
    {
        //Debug.Log("[TRIGGER]: " + name + " SetState " + activate);
        isActivated = activate;
        StartTimer();
        stateUpdated.Invoke();
    }

    private void StartTimer()
    {
        if (settings.timerMode != TimerMode.NONE && isActivated == !timerTargetState && settings.time > 0)
        {
            timeLeft = settings.time;
        }
    }

    private void Update()
    {
        UpdateInitialState();
        UpdateTimer();
        //Debug.Log("[TRIGGER]: " + name + " isActivated is " + isActivated.ToString().ToUpper());
    }

    private void UpdateTimer()
    {
        if (settings.timerMode != TimerMode.NONE && isActivated == !timerTargetState && settings.time > 0 && timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0) SetState(timerTargetState);
        }
    }

    private void UpdateInitialState()
    {
        if (!Application.isPlaying)
        {
            if (!settings.controlledBySwitch) SetState(settings.initActive);
        }
    }

    public void AddListener(UnityAction call)
    {

        stateUpdated.AddListener(call);
    }

    public void RemoveListener(UnityAction call)
    {
        stateUpdated.RemoveListener(call);
    }
}
