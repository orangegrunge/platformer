using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using System.Linq;

public enum SwitchLogic
{
    OR,
    AND,
    XOR
}

[ExecuteInEditMode]
public class Switch : MonoBehaviour
{
    [Header("Switch Settings")]
    public SwitchLogic switchLogic;
    public bool invertState;
    public List<Trigger> triggers = new List<Trigger>();
    public UnityEvent onActivation;
    public UnityEvent onDeactivation;

    //[HideInInspector]
    bool isActivated = false;
    bool oldState = false;

    GameSettings gs;

    private void Awake()
    {
        if (onActivation == null) onActivation = new UnityEvent();
        if (onActivation == null) onDeactivation = new UnityEvent();
    }

    private void Start()
    {
        gs = FindObjectOfType<GameSettings>();
        triggers = triggers.FindAll(x => x != null);
        triggers.ForEach(x => x.AddListener(UpdateStateByTrigger));
        UpdateState(true);
        InvokeEvents();
    }

    public void Update()
    {
        if (!gs.gameStarted)
        {
            UpdateState(true);
        }
    }

    public void SetTriggersByStepChild(StepChild2 stepChild)
    {
        triggers = new List<Trigger>();
        if (stepChild && stepChild.Parent) {
            triggers.Add(stepChild.Parent.GetComponent<Trigger>());
        }

    }

    public void UpdateStateByTrigger()
    {
        //Debug.Log("[SWITCH]: " + "UpdateStateByTrigger " + name);
        if (Application.isPlaying) UpdateState(false);
    }

    public void UpdateState(bool forced)
    {
        oldState = isActivated;

        
        if (triggers.Count == 0)
        {
            isActivated = true;
        }
        else if (switchLogic == SwitchLogic.AND)
        {
            isActivated = triggers.TrueForAll(x => x && x.isActivated);
        }
        else if (switchLogic == SwitchLogic.XOR)
        {
            isActivated = triggers.Exists(x => x && x.isActivated) && !triggers.TrueForAll(x => x && x.isActivated);
        }
        else
        {
            isActivated = triggers.Exists(x => x && x.isActivated);
        }
        

        if (forced || oldState != isActivated)
        {
            InvokeEvents();
        }
    }

    private void InvokeEvents()
    {
        //Debug.Log("[SWITCH]: " + name + " is Invoking" + (isActivated != invertState));
        if (isActivated != invertState) onActivation.Invoke();
        else onDeactivation.Invoke();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        triggers.ForEach(x => {
            if (x)
            {
                Gizmos.color = x.gameObject == gameObject ? Color.gray : Color.white;
                Gizmos.DrawLine(transform.position, x.transform.position);
            }
        });
        Handles.color = Color.red;
        
        if (triggers.Contains(null)) {
            GUIStyle red = new GUIStyle();
            red.normal.textColor = Color.red;
            Handles.Label((Vector2)transform.position + Vector2.up * 1.2f + Vector2.right * 0.2f, "SOME TRIGGERS ARE NULL", red);
        }
        Handles.Label((Vector2)transform.position + Vector2.up * 0.8f + Vector2.right * 0.2f, switchLogic.ToString());
    }
#endif
}
