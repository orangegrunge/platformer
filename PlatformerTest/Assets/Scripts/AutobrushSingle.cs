using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
namespace UnityEditor.Tilemaps
{
    [CustomGridBrush(true, false, false, "AutoBrush_1x1")]
    public class AutoBrushSingle : GridBrush
    {
        Tilemap targetMap;
        Tilemap palette;

        public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position)
        {
            targetMap = brushTarget.GetComponent<Tilemap>();
            TileBase tile = targetMap.GetTile(position);

            base.Paint(grid, brushTarget, position);

            if (tile != targetMap.GetTile(position))
            {
                palette = GridPaintingState.palette.GetComponentInChildren<Tilemap>();
                UpdateTiles(position);
            }
        }

        public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            targetMap = brushTarget.GetComponent<Tilemap>();
            TileBase tile = targetMap.GetTile(position);

            base.Erase(gridLayout, brushTarget, position); ;

            if (tile != targetMap.GetTile(position))
            {
                palette = GridPaintingState.palette.GetComponentInChildren<Tilemap>();
                UpdateTiles(position);
            }
        }


        private void UpdateTiles(Vector3Int c)
        {
            if (targetMap.HasTile(c)) UpdateTile(c);
        }

        private void UpdateTile(Vector3Int pos)
        {
            List<string> tileCodes = TileTool.GetTileCodes();
            string cellsCode = "";
            for (int y = pos.y - 1; y < pos.y + 2; y++)
            {
                for (int x = pos.x - 1; x < pos.x + 2; x++)
                {
                    Vector3Int p = new Vector3Int(x, y, 0);
                    cellsCode = cellsCode + (targetMap.HasTile(p) ? "1" : "0");
                }
            }
            string restrictedChars = GetRestrictedChars(cellsCode);

            string tileCode = GetTargetTileCode(restrictedChars, tileCodes);
            int index = tileCodes.FindIndex(x => { return x == tileCode; });
            string[] splitAdress = TileTool.order[index].Split(' ');
            Vector3Int adress = new Vector3Int(int.Parse(splitAdress[1]), int.Parse(splitAdress[2]), 0);
            TileBase tile = palette.GetTile(adress);
            targetMap.SetTile(pos, tile);
        }

        private string GetRestrictedChars(string cCode)
        {
            string[] rules = new string[] {
                "1 7 _",
                "2 5 _",
                "3 1 _",
                "4 3 _",
                "5 _ 7",
                "6 _ 5",
                "7 _ 1",
                "8 _ 3",
                "A 73 _",
                "B 75 _",
                "C 51 _",
                "D 13 _",
                "E 6 73",
                "F 8 75",
                "G 2 51",
                "H 0 13",
                "I 3 7",
                "J 5 7",
                "K 5 1",
                "L 3 1",
                "M 7 3",
                "N 7 5",
                "O 1 5",
                "P 1 3",
                "Q _ 367",
                "R _ 785",
                "S _ 521",
                "T _ 103"
            };
            string restrictedChars = "";
            foreach (string rule in rules)
            {
                var split = rule.Split(' ');
                char character = split[0][0];
                string restrictions = split[1][0] == '_' ? "" : split[1];
                string requirements = split[2][0] == '_' ? "" : split[2];

                foreach (char r in restrictions)
                {
                    if (cCode[int.Parse(r.ToString())] == '1')
                    {
                        restrictedChars = restrictedChars + character;
                        break;
                    }
                }

                if (restrictedChars.Length > 0 && restrictedChars[restrictedChars.Length - 1] == character) continue;
                foreach (char r in requirements)
                {
                    if (cCode[int.Parse(r.ToString())] != '1')
                    {
                        restrictedChars = restrictedChars + character;
                        break;
                    }
                }
            }
            return restrictedChars;
        }

        private string GetTargetTileCode(string restrictedChars, List<string> tileCodes)
        {
            List<string> result = new List<string>();

            foreach (string tileCode in tileCodes)
            {
                bool skip = false;
                foreach (char r in restrictedChars)
                {
                    if (tileCode.Contains(r.ToString()))
                    {
                        skip = true;
                        break;
                    }
                }
                if (!skip) result.Add(tileCode);
            }

            if (result.Count == 1) return result[0];
            throw new Exception("restrictedChars: " + restrictedChars + ", result: [" + string.Join(", ", result) + "]");
        }
    }

    [CustomEditor(typeof(AutoBrushSingle))]
    public class AutoBrushSingleEditor : GridBrushEditor
    {
        private AutoBrushSingle autoBrush { get { return target as AutoBrushSingle; } }
        public override void OnPaintSceneGUI(GridLayout grid, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
        {
            base.OnPaintSceneGUI(grid, brushTarget, position, tool, executing);

        }
    }
}
#endif