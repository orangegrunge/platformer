using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Controller2D : RaycastController
{
    [SerializeField]
    //[HideInInspector]
    public CollisionInfo collisions;
    [HideInInspector]
    public Vector2 playerInput;

    PlayerController player;
    HazardReaction hazardReaction;
    List<string> collisionPriority = new List<string>() {
        "Level",
        "Box",
        "Untagged",
        "Hazard"
    };

    bool PriorityIsHigher(string current, string suggested)
    {
        return collisionPriority.FindIndex(x => x == current) 
            > collisionPriority.FindIndex(x => x == suggested);
    }

    public override void Start()
    {
        base.Start();
        collisions.faceDir = 1;
        player = GetComponent<PlayerController>();
        hazardReaction = GetComponent<HazardReaction>();
    }

    public void Move(Vector2 moveAmount, Vector2 input, bool standingOnPlatform = false)
    {
        UpdateRaycastOrigins();

        collisions.Reset();
        playerInput = input;

        if (moveAmount.x != 0)
        {
            collisions.faceDir = (int)Mathf.Sign(moveAmount.x);
        }

        moveAmount = GetPossibleMoveAmount(moveAmount);

        transform.Translate(moveAmount);

        if (standingOnPlatform)
        {
            collisions.below = true;
        }
    }

    public Vector2 GetPossibleMoveAmount(Vector2 moveAmount)
    {
        HorizontalCollisions(ref moveAmount);
        if (moveAmount.y != 0)
        {
            VerticalCollisions(ref moveAmount);
        }

        return moveAmount;
    }

    void HorizontalCollisions(ref Vector2 moveAmount)
    {
        float directionX = collisions.faceDir;
        float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

        if (Mathf.Abs(moveAmount.x) < skinWidth)
        {
            rayLength = 2 * skinWidth;
        }
        RaycastHit2D hit = new RaycastHit2D();
        for (int i = 0; i < horizontalRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);
            //RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            List<RaycastHit2D> hits = new List<RaycastHit2D>(Physics2D.RaycastAll(rayOrigin, Vector2.right * directionX, rayLength, collisionMask));

            hits.Sort((a, b) => (a.distance < b.distance) ? 1 : -1);
            
            hits = hits.FindAll(x => x.collider != collider);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.red);
            if (hits.Count > 0)
            {
                DrawCross(hits[0].point);
                bool isBox = hits[0].collider.tag == "Box";
                bool isPlayer = player;

                if (hits[0].distance == 0 && !isBox)
                {

                    continue;
                }

                if (isBox && isPlayer)
                {
                    //Debug.Log("MoveAmout before box hit: " + moveAmount);
                    Box box = hits[0].collider.GetComponent<Box>();
                    moveAmount.x = box.GetPossibleMoveAmount(new Vector2(moveAmount.x, 0)).x;

                    Vector2 input = Vector2.zero;
                    input.x = player.dashing ? player.dashSpeed * player.dashDirection : playerInput.x;

                    box.SetDirectionalInput(input);

                    //Debug.Log("MoveAmout after box hit: " + moveAmount);

                }
                else
                {
                    moveAmount.x = (hits[0].distance - skinWidth) * directionX;
                }

                //rayLength = hits[0].distance;

                collisions.left = directionX == -1;
                collisions.right = directionX == 1;
                hit = CheckCollisionPriority(hit, hits);
            }
        }
        CheckHazard(hit);
    }

    void VerticalCollisions(ref Vector2 moveAmount)
    {
        float directionY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;
        RaycastHit2D hit = new RaycastHit2D();

        for (int i = 0; i < verticalRayCount; i++)
        {

            Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);

            List<RaycastHit2D> hits = new List<RaycastHit2D>(Physics2D.RaycastAll(rayOrigin, Vector2.up * directionY, rayLength, collisionMask));

            hits.Sort((a, b) => (a.distance < b.distance) ? 1 : -1);

            hits = hits.FindAll(x => x.collider != collider);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.red);

            if (hits.Count > 0)
            {
                DrawCross(hits[0].point);
                if (hits[0].collider.tag == "Through")
                {
                    if (directionY == 1 || hits[0].distance == 0)
                    {
                        continue;
                    }
                    if (collisions.fallingThroughPlatform)
                    {
                        continue;
                    }
                    if (playerInput.y == -1)
                    {
                        collisions.fallingThroughPlatform = true;
                        Invoke("ResetFallingThroughPlatform", .5f);
                        continue;
                    }
                }

                moveAmount.y = (hits[0].distance - skinWidth) * directionY;
                rayLength = hits[0].distance;

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;

                hit = CheckCollisionPriority(hit, hits);
            }
        }
        CheckHazard(hit);

    }

    private RaycastHit2D CheckCollisionPriority(RaycastHit2D hit, List<RaycastHit2D> hits)
    {
        if (!hit) hit = hits[0];
        else
        {
            if (hit.collider && PriorityIsHigher(hit.collider.tag, hits[0].collider.tag))
            {
                hit = hits[0];
            }
        }

        return hit;
    }


    private void CheckHazard(RaycastHit2D hit)
    {
        if (hit.collider && hit.collider.tag == "Hazard")
        {
            hazardReaction.React(hit);
        }
    }



    [System.Serializable]
    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public RaycastHit2D aboveT, belowT;
        public RaycastHit2D leftT, rightT;

        public int faceDir;
        public bool fallingThroughPlatform;

        public void Reset()
        {
            above = below = false;
            left = right = false;
            aboveT = belowT = new RaycastHit2D();
            leftT = rightT = new RaycastHit2D();
        }
    }

    public void DrawCross(Vector3 pos, float size = 0.05f)
    {
        Debug.DrawLine(pos - new Vector3(size, size), pos + new Vector3(size, size));
        Debug.DrawLine(pos - new Vector3(-size, size), pos + new Vector3(-size, size));
    }
}
