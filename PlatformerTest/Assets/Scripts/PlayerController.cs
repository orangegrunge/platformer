using System;
using System.Collections;
using System.Collections.Generic;
using Steer2D;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [Header("Settings")]
    public float jumpHeight;
    public float jumpLength;
    public float moveSpeed;
    public float jumpQueueWindow;
    public int jumpCharges;
    public int dashCharges;
    public float dashLength;

    public float dashTime;

    public float invulnerabilityTime;

    [HideInInspector] public float gravity;
    [HideInInspector] public float maxJumpVelocity;
    Vector3 velocity;
    bool queuedJump;
    int currentJumpCharges = 1;
    [HideInInspector] public bool jumping;
    [HideInInspector] public bool dashing;
    [HideInInspector] public int dashDirection = 1;
    int currentDashCharges;
    [HideInInspector] public float dashSpeed;

    [HideInInspector] public Controller2D controller;
    ActionHint actionHint;
    [HideInInspector] public ActionSpot actionSpot;

    [Header("Refs")]
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    LevelStateManager stateManager;

    Vector2 directionalInput = new Vector2();
    [HideInInspector] public bool blink;
    private int blinkTimer;
    internal SteeringAgent steeringAgent;
    public List<AudioClip> hurtClips;
    public List<AudioClip> jumpClips;
    public AudioSource audioSource;


    // COMMON
    void Start()
    {
        controller = GetComponent<Controller2D>();
        actionHint = FindObjectOfType<ActionHint>();

        gravity = -8 * jumpHeight * Mathf.Pow(moveSpeed, 2) / Mathf.Pow(jumpLength, 2);
        maxJumpVelocity = 4 * jumpHeight * moveSpeed / jumpLength;
        dashSpeed = dashLength / dashTime;

        stateManager = FindObjectOfType<LevelStateManager>();
        steeringAgent = GetComponent<Steer2D.SteeringAgent>();
    }

    void Update()
    {
        if (stateManager.loose) return;
        GetActionSpot();
        ResetJumpCharges();
        DoQueuedJump();

        ResetDashCharges();
        

        CalculateVelocity();

        controller.Move(velocity * Time.deltaTime, directionalInput);

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }
        Animate();
    }

    void CalculateVelocity()
    {
        if (dashing)
        {
            velocity.x = dashSpeed * dashDirection;
            velocity.y = 0;
        }
        else
        {
            float targetVelocityX = directionalInput.x * moveSpeed;
            velocity.x = targetVelocityX;
            velocity.y += gravity * Time.deltaTime;
            velocity.y = Mathf.Clamp(velocity.y, -maxJumpVelocity, maxJumpVelocity);
        }
    }

    void Animate()
    {
        spriteRenderer.flipX = controller.collisions.faceDir < 0;

        animator.SetBool("run", Mathf.Abs(velocity.x) > 0f);
        animator.SetBool("air_up", velocity.y > 0);
        animator.SetBool("air_down", velocity.y < 0);
    }

    // JUMP LOGIC
    private void DoQueuedJump()
    {
        if (controller.collisions.below && queuedJump) Jump();
    }

    internal void SkipJumpQueue()
    {
        queuedJump = false;
    }

    private void Jump()
    {
        MakeJumpSound();
        CancelInvoke("ResetDash");
        dashing = false;
        velocity.y = maxJumpVelocity;
    }

    private void ResetJumpCharges()
    {
        if (controller.collisions.below) currentJumpCharges = jumpCharges;
    }

    internal void SetDirectionalInput(Vector2 input)
    {
        directionalInput = input;
    }

    internal void OnJumpInputDown()
    {
        if (controller.collisions.below)
        {
            Jump();
        }
        else if (currentJumpCharges > 0)
        {
            Jump();
            currentJumpCharges--;
        }
        else
        {
            queuedJump = true;
            Invoke("SkipJumpQueue", jumpQueueWindow);
        }
    }
    
    internal void OnJumpInputHold()
    {

    }

    internal void OnJumpInputUp()
    {
        queuedJump = false;
    }


    //DASH LOGIC
    internal void OnDashInputHold()
    {
        //throw new NotImplementedException();
    }

    internal void OnDashInputUp()
    {
        //throw new NotImplementedException();
    }

    internal void OnDashInputDown()
    {
        if (!dashing && currentDashCharges > 0)
        {
            currentDashCharges--;
            dashing = true;
            dashDirection = controller.collisions.faceDir;
            Invoke("ResetDash", dashTime);
        }
    }

    void ResetDash()
    {
        dashing = false;
    }

    private void ResetDashCharges()
    {
        if (controller.collisions.below && !dashing) currentDashCharges = dashCharges;
    }


    // ACTION LOGIC
    internal void OnActionInputDown()
    {
        Debug.Log("Action");
        if (actionSpot) {
            
            actionSpot.OnActionKeyDown();
        }
    }

    private void GetActionSpot()
    {
        List<Collider2D> colliders = new List<Collider2D>();
        ContactFilter2D filter = new ContactFilter2D();

        controller.collider.OverlapCollider(filter, colliders);
        colliders = colliders.FindAll(x => x.tag == "ActionSpot");
        if (colliders.Count > 0)
        {
            Collider2D result = colliders[0];
            float resultDistance = Vector2.SqrMagnitude((Vector2)result.transform.position - (Vector2)transform.position);
            for (int i = 1; i<colliders.Count; i++)
            {
                float newDistance = Vector2.SqrMagnitude((Vector2)colliders[i].transform.position - (Vector2)transform.position);
                if (newDistance < resultDistance)
                {
                    result = colliders[i];
                    resultDistance = newDistance;
                }
            }
            actionSpot = result.GetComponent<ActionSpot>();
        }
        else
        {
            actionSpot = null;
        }

        UpdateActionHint();
    }

    private void UpdateActionHint()
    {
        if (actionSpot && actionSpot.active) actionHint.Set(true, actionSpot.hintPosition.position);
        else actionHint.Set(false, Vector2.zero);
    }

    internal void GetHit(bool oneHit)
    {
        if (stateManager.win) return;
        stateManager.GetHit(oneHit);
        Blink();
        MakeHurtSound();
    }

    public void Blink()
    {
        blink = true;
        spriteRenderer.color = new Color(1,1,1,0.5f);
        StartCoroutine(SkipBlink());
    }

    IEnumerator SkipBlink()
    {
        yield return new WaitForSeconds(invulnerabilityTime);
        blink = false;
        spriteRenderer.color = new Color(1, 1, 1, 1);
    }
    

    public void Die()
    {
        animator.SetTrigger("die");
        Destroy(controller);
        Destroy(this);
    }

    private void MakeJumpSound()
    {
        PlayRandomSound(jumpClips);
    }

    private void PlayRandomSound(List<AudioClip> clips)
    {
        if (clips.Count > 0)
        {
            int index = UnityEngine.Random.Range(0, clips.Count);
            audioSource.PlayOneShot(clips[index]);
        }
    }

    private void MakeHurtSound()
    {
        PlayRandomSound(hurtClips);
    }
}
