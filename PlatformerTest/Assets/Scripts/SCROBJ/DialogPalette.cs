using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogPaletteInstance
{
    public string tag;
    public Color panel = new Color(1,1,1,0);
    public Color frameFace = new Color(1, 1, 1, 0);
    public Color imageFace = new Color(1, 1, 1, 0);
    public Color frameText = new Color(1, 1, 1, 0);
    public Color text = new Color(1, 1, 1, 0);
    public float textSize = 10;
}

[CreateAssetMenu(fileName = "DialogPalette", menuName = "ScriptableObjects/DialogPalette", order = 1)]
public class DialogPalette : ScriptableObject
{
    public List<DialogPaletteInstance> palette;

    public DialogPaletteInstance GetPalette(string paletteTag)
    {
        var p = palette.Find(x => x.tag == paletteTag);
        return p;
    }
}
