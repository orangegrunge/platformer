using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogSpeech
{
    public string triggerTag;
    public string nextSpeechTag;
    public List<DialogLine> lines;

}

[System.Serializable]
public class DialogLine
{
    public Sprite face;
    [Multiline]
    public string text;
    public AudioClip voice;
    public string palette;


}

[CreateAssetMenu(fileName = "DialogScript", menuName = "ScriptableObjects/DialogScript", order = 1)]
public class DialogScript : ScriptableObject
{
    public List<DialogSpeech> speeches;

    public DialogSpeech GetSpeech(string triggerTag)
    {
        var speech = speeches.Find(x => x.triggerTag == triggerTag);
        return speech; 
    }
}
