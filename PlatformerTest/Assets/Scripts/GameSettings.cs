using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class GameSettings : MonoBehaviour
{


    [HideInInspector]
    public bool gameStarted;


    private void Start()
    {
        gameStarted = true;
    }


}
