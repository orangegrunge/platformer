using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class InputController : MonoBehaviour
{
    public KeyCode jump;
    public KeyCode dash;
    public KeyCode action;

    PlayerController player;
    SessionManager sessionManager;

    bool locked;

    void Start()
    {
        sessionManager = FindObjectOfType<SessionManager>();
        player = GetComponent<PlayerController>();
    }

    void Update()
    {
        if (locked) return;
        if (player)
        {
            InputDirections();
            InputJump();
            InputDash();
            InputAction();
        }
        InputRestart();
    }

    private void InputDirections()
    {
        Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        player.SetDirectionalInput(directionalInput);
    }

    private void InputJump()
    {
        if (Input.GetKeyDown(jump))
        {
            player.OnJumpInputDown();
        }
        else if (Input.GetKey(jump))
        {
            player.OnJumpInputHold();
        }
        else if (Input.GetKeyUp(jump))
        {
            player.OnJumpInputUp();
        }
    }

    private void InputDash()
    {
        if (Input.GetKeyDown(dash))
        {
            player.OnDashInputDown();
        }
        else if (Input.GetKey(dash))
        {
            player.OnDashInputHold();
        }
        else if (Input.GetKeyUp(dash))
        {
            player.OnDashInputUp();
        }
    }

    private void InputAction()
    {
        if (Input.GetKeyDown(action))
        {
            player.OnActionInputDown();
        }
    }

    private void InputRestart()
    {
        
    }

    public void Lock()
    {
        locked = true;
    }

    public void Unlock()
    {
        locked = false;
    }
}
