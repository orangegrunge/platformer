using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

#if UNITY_EDITOR
[ExecuteInEditMode]
public class TileTool : MonoBehaviour
{
    public string paletteName;
    public Sprite templateSprite; public bool ordered;

    internal static List<string> order = new List<String> {
        "0 8 1",
        "1 2 1",
        "2 6 3",
        "3 0 4",
        "4 4 7",
        "5 3 0",
        "6 3 1",
        "7 6 5",
        "8 0 5",
        "9 3 7",
        "10 2 0",
        "11 4 1",
        "12 6 4",
        "13 0 3",
        "14 2 7",
        "15 8 7",
        "16 2 2",
        "17 4 2",
        "18 3 2",
        "19 8 8",
        "20 1 5",
        "21 1 4",
        "22 1 3",
        "23 9 7",
        "24 4 6",
        "25 2 6",
        "26 3 6",
        "27 8 6",
        "28 5 3",
        "29 5 5",
        "30 5 4",
        "31 7 7",
        "32 5 6",
        "33 9 8",
        "34 7 1",
        "35 1 6",
        "36 7 8",
        "37 5 2",
        "38 9 6",
        "39 8 2",
        "40 1 2",
        "41 7 6",
        "42 9 1",
        "43 8 0",
        "44 6 1",
        "45 8 3",
        "46 3 4",
    };

    Texture2D templateTexture;
    Vector2Int tSize;
    RectInt innerRect;
    Dictionary<string, RectInt> parts;
    Dictionary<string, TChunk> chunks;
    string[] tileCodes;

    struct TChunk
    {
        public Texture2D tex;
        public RectInt rect;
    }

    [ContextMenu("Combine Palette")]
    public void CombinePalette()
    {
        templateTexture = templateSprite.texture;
        tSize = new Vector2Int(templateTexture.width / 6, templateTexture.height);

        SetInnerRect();
        SetParts();
        SetChunks();
        Texture2D result = CombineTiles();
        SavePaletteToPNG(result);

        paletteName = "";
        templateSprite = null; 
    }

    private void SavePaletteToPNG(Texture2D result)
    {
        var bytes = result.EncodeToPNG();
        File.WriteAllBytes("Assets/Resources/"+ paletteName + ".png", bytes);
        UnityEditor.AssetDatabase.Refresh();
        Sprite paletteSprite = Resources.Load<Sprite>(paletteName);
        
    }

    private void SetInnerRect()
    {
        innerRect = new RectInt();

        for (int y = 0; y < tSize.y; y++)
        {
            if (innerRect.y == 0)
            {
                if (templateTexture.GetPixel(tSize.x, y).a != 0)
                {
                    innerRect.y = y;
                }
            }
            else
            {
                if (templateTexture.GetPixel(tSize.x, y).a == 0)
                {
                    innerRect.height = y - innerRect.y;
                    break;
                }
            }
        }

        for (int x = (int)tSize.x; x < tSize.x*2; x++)
        {
            if (innerRect.x == 0)
            {
                if (templateTexture.GetPixel(x, 0).a != 0)
                {
                    innerRect.x = x - tSize.x;
                }
            }
            else
            {
                if (templateTexture.GetPixel(x, 0).a == 0)
                {
                    innerRect.width = x - tSize.x - innerRect.x;
                    break;
                }
            }
        }
    }

    private void SetChunks() {
        chunks = new Dictionary<string, TChunk>();

        chunks.Add("0", GetChunk("c", 0));

        chunks.Add("1", GetChunk("t", 1));
        chunks.Add("2", GetChunk("r", 1));
        chunks.Add("3", GetChunk("b", 1));
        chunks.Add("4", GetChunk("l", 1));

        chunks.Add("5", GetChunk("t", 0));
        chunks.Add("6", GetChunk("r", 0));
        chunks.Add("7", GetChunk("b", 0));
        chunks.Add("8", GetChunk("l", 0));

        chunks.Add("A", GetChunk("x", 2));
        chunks.Add("B", GetChunk("y", 2));
        chunks.Add("C", GetChunk("z", 2));
        chunks.Add("D", GetChunk("w", 2));

        chunks.Add("E", GetChunk("x", 3));
        chunks.Add("F", GetChunk("y", 3));
        chunks.Add("G", GetChunk("z", 3));
        chunks.Add("H", GetChunk("w", 3));

        chunks.Add("I", GetChunk("x", 4));
        chunks.Add("J", GetChunk("y", 4));
        chunks.Add("K", GetChunk("z", 4));
        chunks.Add("L", GetChunk("w", 4));

        chunks.Add("M", GetChunk("x", 5));
        chunks.Add("N", GetChunk("y", 5));
        chunks.Add("O", GetChunk("z", 5));
        chunks.Add("P", GetChunk("w", 5));

        chunks.Add("Q", GetChunk("x", 0));
        chunks.Add("R", GetChunk("y", 0));
        chunks.Add("S", GetChunk("z", 0));
        chunks.Add("T", GetChunk("w", 0));
    }

    private TChunk GetChunk(string key, int tileOffset)
    {
        TChunk chunk = new TChunk();
        RectInt part = parts[key];
        chunk.rect = part;
        chunk.tex = new Texture2D(part.width, part.height);
        RectInt offset = GetOffseted(key, tileOffset);
        Color[] pixels = templateTexture.GetPixels(offset.x, offset.y, offset.width, offset.height);
        chunk.tex.SetPixels(0, 0, part.width, part.height, pixels);
        return chunk;
    }

    private void SetParts()
    {
        int[] ws = new int[] {innerRect.x, innerRect.width, tSize.x - innerRect.xMax };
        int[] hs = new int[] {innerRect.y, innerRect.height, tSize.y - innerRect.yMax };
        int[] xs = new int[] {0, innerRect.x, innerRect.x + innerRect.width, tSize.x };
        int[] ys = new int[] {0, innerRect.y, innerRect.y + innerRect.height, tSize.y };


        parts = new Dictionary<string, RectInt>();

        parts.Add("c", new RectInt(xs[0], ys[0], tSize.x, tSize.y));

        parts.Add("t", new RectInt(xs[1], ys[2], ws[1], hs[2]));
        parts.Add("r", new RectInt(xs[2], ys[1], ws[2], hs[1]));
        parts.Add("b", new RectInt(xs[1], ys[0], ws[1], hs[0]));
        parts.Add("l", new RectInt(xs[0], ys[1], ws[0], hs[1]));

        parts.Add("x", new RectInt(xs[0], ys[2], ws[0], hs[2]));
        parts.Add("y", new RectInt(xs[2], ys[2], ws[2], hs[2]));
        parts.Add("z", new RectInt(xs[2], ys[0], ws[2], hs[0]));
        parts.Add("w", new RectInt(xs[0], ys[0], ws[0], hs[0]));
    }

    private RectInt GetOffseted(string key, int tileOffset)
    {
        int po = tileOffset * tSize.x;
        RectInt part = parts[key];
        return new RectInt(part.x + po, part.y, part.width, part.height);
    }

    private void AddChunk(Texture2D tex, string key)
    {
        TChunk chunk = chunks[key];
        Color[] pixels = chunk.tex.GetPixels();
        RectInt r = chunk.rect;
        tex.SetPixels(r.x, r.y, r.width, r.height, pixels);
    }

    private Texture2D CombineTile(string tileCode)
    {
        Texture2D result = new Texture2D(tSize.x, tSize.y);
        AddChunk(result, "0");
        foreach (char c in tileCode)
        {
            AddChunk(result, c.ToString());
        }
        return result;
    }

    public static List<string> GetTileCodes()
    {
        string[] sides = new string[] {
            "5678","1678","2578","3568",
            "4567","1278","1368","1467",
            "2358","2457","3456","1238",
            "2345","3416","4127","1234"
        };

        string[] corners = new string[] {
            "AEIMQ","BFJNR","CGKOS","DHLPT"
        };

        List<string> tileCodes = new List<string>();

        foreach (string sidesVarint in sides)
        {
            foreach (char x in corners[0])
            {
                if (IsRestricted(x, sidesVarint)) continue;
                foreach (char y in corners[1])
                {
                    if (IsRestricted(y, sidesVarint)) continue;
                    foreach (char z in corners[2])
                    {
                        if (IsRestricted(z, sidesVarint)) continue;
                        foreach (char w in corners[3])
                        {
                            if (IsRestricted(w, sidesVarint)) continue;
                            tileCodes.Add(sidesVarint + x + y + z + w);
                        }
                    }
                }
            }
        }
        return tileCodes;
    }

    private static bool IsRestricted(char corner,  string sidesVariant)
    {
        string[] restrictions = new string[] {
            "A58","B56","C67","D78",
            "E14","F12","G23","H34",
            "I18","J16","K36","L38",
            "M45","N25","O27","P47",
            "Q14","R12","S23","T43"
        };

        foreach (string e in restrictions)
        {
            if (e[0] == corner)
            {
                for (int i = 1; i < e.Length; i++)
                {
                    if (sidesVariant.Contains(e[i].ToString()))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }

    private Texture2D CombineTiles()
    {
        List<string> tileCodes = GetTileCodes();
        Texture2D result;
        List<Texture2D> tiles = new List<Texture2D>();

        tileCodes.ForEach(code => tiles.Add(CombineTile(code)));

        if (ordered)
        {
            result = new Texture2D(10 * tSize.x, 10 * tSize.y);
            FillTexture(result);

            order.ForEach(s => {
                var split = s.Split(' ');
                Vector2Int adress = new Vector2Int(int.Parse(split[1]), int.Parse(split[2]));
                Vector2Int pAdress = new Vector2Int(adress.x * tSize.x, adress.y * tSize.y);
                Color[] pixels = tiles[int.Parse(split[0])].GetPixels();
                result.SetPixels(pAdress.x, pAdress.y, tSize.x, tSize.y, pixels);
            });

        }
        else
        {
            result = new Texture2D(7 * tSize.x, 7 * tSize.y);
            FillTexture(result);
            for (int t = 0; t < tiles.Count; t++)
            {
                int y;
                int x = Math.DivRem(t, 7, out y);
                Vector2Int adress = new Vector2Int(x, y);
                Vector2Int pAdress = new Vector2Int(adress.x * tSize.x, adress.y * tSize.y);
                Color[] pixels = tiles[t].GetPixels();
                result.SetPixels(pAdress.x, pAdress.y, tSize.x, tSize.y, pixels);
            }
        }
        
        result.Apply();
        GetComponent<SpriteRenderer>().sprite = Sprite.Create(result, new Rect(0, 0, result.width, result.height), Vector2.zero);
        return result;
    }
    
    private void FillTexture(Texture2D tex)
    {
        Color[] pixels = new Color[tex.width * tex.height];
        for (int i=0; i<pixels.Length; i++)
        {
            pixels[i] = new Color(0, 0, 0, 0);
        }
        tex.SetPixels(pixels);
    }
}
#endif