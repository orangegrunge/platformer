using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Trigger.TriggerSettings))]
public class TimeSettingsDrawer : PropertyDrawer
{
    private bool showTimer;
    private bool isPassive;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var controlledBySwitch = property.FindPropertyRelative("controlledBySwitch");
        isPassive = controlledBySwitch.boolValue;
        EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), controlledBySwitch);

        if (!isPassive)
        {
            var initActive = property.FindPropertyRelative("initActive");
            position.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), initActive);

            var timerMode = property.FindPropertyRelative("timerMode");
            position.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), timerMode);

            showTimer = timerMode.intValue != (int)Trigger.TimerMode.NONE;
            if (showTimer)
            {
                position.y += EditorGUIUtility.singleLineHeight;

                var time = property.FindPropertyRelative("time");
                EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), time);
            }
        }
        
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {

        var height = 1;
        if (!isPassive)
        {
            height += 2;
            if (showTimer) height++;
        }

        return height * EditorGUIUtility.singleLineHeight;
    }
}

[CustomPropertyDrawer(typeof(PathController.PathSettings))]
public class PathSettingsDrawer : PropertyDrawer
{
    private bool showSpeedSettings;
    private bool showStartPositionSettings;
    private int height;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        height = 1;

        var pathLogic = property.FindPropertyRelative("pathLogic");
        EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), pathLogic);

        var initMoving = property.FindPropertyRelative("initMoving");
        position = NewLine(position);
        EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), initMoving);

        var straightDirection = property.FindPropertyRelative("straightDirection");
        position = NewLine(position);
        EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), straightDirection);

        position = NewLine(position);
        var m_showSpeedSettings = property.FindPropertyRelative("m_showSpeedSettings");
        m_showSpeedSettings.boolValue = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight),
            m_showSpeedSettings.boolValue, "Speed Options");
        if (m_showSpeedSettings.boolValue)
        {
            var speed = property.FindPropertyRelative("speed");
            position = NewLine(position);
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), speed);

            var speedCurve = property.FindPropertyRelative("speedCurve");
            position = NewLine(position);
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), speedCurve);
        }

        position = NewLine(position);
        var m_showStartPositionSettings = property.FindPropertyRelative("m_showStartPositionSettings");
        m_showStartPositionSettings.boolValue = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight),
            m_showStartPositionSettings.boolValue, "Position Options");
        if (m_showStartPositionSettings.boolValue)
        {
            var segment = property.FindPropertyRelative("segment");
            position = NewLine(position);
            var segmentsCount = property.FindPropertyRelative("segmentsCount");
            segment.intValue = (int)EditorGUI.Slider(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), new GUIContent("Segment"), segment.intValue, 0, segmentsCount.intValue);

            var segmentPart = property.FindPropertyRelative("segmentPart");
            position = NewLine(position);
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), segmentPart);
        }
    }

    private Rect NewLine(Rect position)
    {
        height++;
        position.y += EditorGUIUtility.singleLineHeight;
        return position;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return height * EditorGUIUtility.singleLineHeight;
    }
}