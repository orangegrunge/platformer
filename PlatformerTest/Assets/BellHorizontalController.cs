using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class BellHorizontalController : MonoBehaviour
{

    [Header("Refs")]
    public Transform ropeLeftTransform;
    public SpriteRenderer ropeLeftSprite;

    public Transform ropeRightTransform;
    public SpriteRenderer ropeRightSprite;

    public Transform bell;
    public Transform leftPoint;
    public Transform rightPoint;
    public Transform rightEnd;

    [Header("Settings")]
    public float speed;
    public bool goRight;

    [Header("Debug")]
    public bool hold;

    LevelStateManager stateManager;

    private void Start()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
    }

    private void Update()
    {


        if (!Application.isPlaying)
        {
            leftPoint.localPosition = new Vector2(Mathf.Clamp(leftPoint.localPosition.x, 0, rightPoint.localPosition.x), 0);
            rightPoint.localPosition = new Vector2(Mathf.Clamp(rightPoint.localPosition.x, 0, rightEnd.localPosition.x), 0);
            rightEnd.localPosition = new Vector2(rightEnd.localPosition.x, 0);
            SetBellPosition();
        }
        else
        {
            MoveBell();
        }
        SetRopePosition();
    }

    private void MoveBell()
    {
        if (stateManager.pause || hold) return;

        if (goRight)
        {
            bell.transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (bell.transform.position.x >= rightPoint.position.x)
            {
                goRight = false;
            }
        }
        else
        {
            bell.transform.Translate(Vector2.left * speed * Time.deltaTime);
            if (bell.transform.position.x <= leftPoint.position.x)
            {
                goRight = true;
            }
        }
    }

    private void SetBellPosition()
    {
        bell.transform.localPosition = new Vector2(Mathf.Clamp(bell.transform.localPosition.x, leftPoint.localPosition.x, rightPoint.localPosition.x), 0);
    }

    private void SetRopePosition()
    {
        ropeLeftTransform.localPosition = bell.transform.localPosition;
        ropeLeftSprite.size = new Vector2(1, (transform.position - ropeLeftTransform.position).magnitude);

        ropeRightTransform.localPosition = bell.transform.localPosition;
        ropeRightSprite.size = new Vector2(1, (ropeRightTransform.position - rightEnd.transform.position).magnitude);
    }
}
