using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockController : MonoBehaviour
{
    public Image image;

    public List<Sprite> sprites;

    LevelStateManager stateManager;
    float stepDuration;
    int currentStep;

    private void Start()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
        stepDuration = stateManager.levelDuration / sprites.Count;
        currentStep = 0;
    }

    public void Update()
    {
        SetImage();
    }

    public void SetImage()
    {
        if ((currentStep + 1) * stepDuration < stateManager.GetTime())
        {
            currentStep++;
            if (currentStep >= sprites.Count) return;
            image.sprite = sprites[currentStep];
        }
        
    }
}
